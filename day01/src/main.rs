// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

use log::{info, log_enabled, trace, Level, LevelFilter};

use std::collections::HashSet;

const INPUTTXT: &str = include_str!("../input01.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> i32 {
    input
        .iter()
        .map(|cc| cc.trim().parse::<i32>().unwrap())
        .sum()
}

fn part2(input: &[&str]) -> i32 {
    let mut haveseen = HashSet::new();
    let mut fr = 0;
    haveseen.insert(fr);
    for n in input
        .iter()
        .map(|cc| cc.trim().parse::<i32>().unwrap())
        .cycle()
    {
        fr += n;
        if !haveseen.insert(fr) {
            return fr;
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use std::io::BufRead;

    use super::*;

    const INPUT01: &str = "\
    +1, -2, +3, +1
    +1, +1, +1
    +1, +1, -2
    -1, -2, -3
        ";

    const WANT01: &[i32] = &[3, 3, 0, -6];

    const INPUT02: &str = "\
    +1, -2, +3, +1
    +1, -1
    +3, +3, +4, -2, -4
    -6, +3, +8, +5, -6
    +7, +7, -2, -7, -4
        ";

    const WANT02: &[i32] = &[2, 0, 10, 5, 14];

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().collect();
        for (n, line) in input.iter().enumerate() {
            if line.trim().is_empty() {
                continue;
            }
            let inp: Vec<&str> = line.split(',').map(|cc| cc.trim()).collect();
            info!("Test {:?} to find {}", &inp, WANT01[n]);
            assert_eq!(WANT01[n], part1(&inp[..]));
        }
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT02.lines().collect();
        for (n, line) in input.iter().enumerate() {
            if line.trim().is_empty() {
                continue;
            }
            let inp: Vec<&str> = line.split(',').map(|cc| cc.trim()).collect();
            info!("Test {:?} to find {}", &inp, WANT02[n]);
            assert_eq!(WANT02[n], part2(&inp[..]));
        }
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
