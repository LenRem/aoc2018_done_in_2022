// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input05.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input = INPUTTXT.trim();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&str) -> R, input: &str) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &str) -> usize {
    reaction(input).len()
}

fn part2(input: &str) -> usize {
    ('a'..='z')
        .inspect(|c| print!("{} --> ", c))
        .map(|c| {
            let clean = remove_type(&input, c);
            reaction(&clean).len()
        })
        .inspect(|n| println!(" {}", n))
        .min()
        .unwrap()
}

// Do a full reaction
fn reaction(inp: &str) -> String {
    let mut inpb = inp.to_string().into_bytes(); // The old string
    let result = loop {
        debug!("{:6}: {}", inpb.len(), std::str::from_utf8(&inpb).unwrap());
        let mut newb = Vec::with_capacity(inpb.len()); // The new string.
        let mut inpbi = inpb.iter().enumerate();
        while let Some((n, chb)) = inpbi.next() {
            if n + 1 == inpb.len() {
                // Last byte: just transfer
                trace!("n: {}, chb: {}, next:  {}", n, chb, "END");
                newb.push(*chb);
            } else {
                trace!("n: {}, chb: {}, next:  {}", n, chb, inpb[n + 1]);
                if inpb[n + 1] == chb + 0x20 || inpb[n + 1] == chb - 0x20 {
                    // Hit. Consume n+1 too.
                    inpbi.next();
                } else {
                    // No hit. Transfer to new.
                    newb.push(*chb);
                }
            }
        }
        // Repeat the process until the new string stops changing
        if newb.len() != inpb.len() && newb.len() > 0 {
            inpb = newb;
        } else {
            break newb;
        }
    };
    String::from_utf8(result).unwrap()
}

fn remove_type(inp: &str, chr: char) -> String {
    let chr2 = chr.to_ascii_uppercase();
    inp.chars().filter(|&c| c != chr && c != chr2).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &[&str] = &["abBA", "abAB", "aabAAB", "dabAcCaCBAcCcaDA"];
    const WANT: [usize; INPUT.len()] = [0, 4, 6, 10];
    const WANT2: [usize; INPUT.len()] = [0, 0, 0, 4];

    /// Test individual shuffling techniques
    #[test]
    fn test1() {
        init_logger();
        for (inp, wnt) in INPUT.iter().zip(WANT.iter()) {
            assert_eq!(*wnt, part1(inp));
        }
    }

    #[test]
    fn test2() {
        init_logger();
        for (inp, wnt) in INPUT.iter().zip(WANT2.iter()) {
            assert_eq!(*wnt, part2(inp));
        }
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
