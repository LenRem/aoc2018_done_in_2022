// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::HashMap; // {HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input02.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().map(|s| s.trim()).filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let mut cnt2 = 0;
    let mut cnt3 = 0;
    for line in input {
        let (has2, has3) = getcnt2cnt3(line);
        cnt2 += if has2 { 1 } else { 0 };
        cnt3 += if has3 { 1 } else { 0 };
    }
    cnt2 * cnt3
}

fn part2(input: &[&str]) -> String {
    for n1 in 0..input.len() {
        for n2 in n1 + 1..input.len() {
            let str1 = input[n1].trim();
            let str2 = input[n2].trim();
            let samesame: String = str1
                .chars()
                .zip(str2.chars())
                .filter(|(c1, c2)| c1 == c2)
                .map(|(c1, _)| c1 )
                .collect();
            if samesame.len() == str1.len() - 1 {
                return samesame;
            }
        }
    }
    "nothing found".to_string()
}

fn getcnt2cnt3(line: &str) -> (bool, bool) {
    let mut ccnt = HashMap::new();
    for c in line.chars() {
        let cnt = ccnt.entry(c).or_insert(0);
        *cnt += 1;
    }
    (
        ccnt.values().any(|v| *v == 2),
        ccnt.values().any(|v| *v == 3),
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    abcdef
    bababc
    abbcde
    abcccd
    aabcdd
    abcdee
    ababab
    ";

    const INPUT02: &str = "\
    abcde
    fghij
    klmno
    pqrst
    fguij
    axcye
    wvxyz
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<&str> = INPUT01.lines().map(|s| s.trim()).filter(|s| !s.is_empty()).collect();
        assert_eq!(12, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<&str> = INPUT02.lines().map(|s| s.trim()).filter(|s| !s.is_empty()).collect();
        assert_eq!("fgij".to_string(), part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
