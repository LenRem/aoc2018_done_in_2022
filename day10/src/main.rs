// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap; //{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input10.txt");

// const LOGLEVEL: LevelFilter = LevelFilter::Info;
const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let map = make_map(input);
    let minsize = i64::MAX;
    let min_n = find_minimum(&map, 0, 1000_000, |n| bboxsz(&pass_time(&map, n)));
    let map_min = pass_time(&map, min_n);
    println!(
        "Step {}: bbsz: {} \n{}",
        min_n,
        bboxsz(&map_min),
        map2string(&map_min)
    );
    min_n
}

fn part2(input: &[&str]) -> usize {
    0
}

type Map = Vec<Light>;

#[derive(Debug, Copy, Clone)]
struct Light {
    pos: Point,
    spd: Point,
}

#[derive(Debug, Copy, Clone)]
struct Point {
    x: i64,
    y: i64,
}

/// Extract all positions and speeds from the textual input lines
/// It is assumed all lines are valid. Panics otherwise.
fn make_map(inp: &[&str]) -> Map {
    inp.iter().map(|s| make_light(s)).collect()
}

fn make_light(inp: &str) -> Light {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r#"position=<\s*(-?\d+),\s*(-?\d*)> velocity=<\s*(-?\d+),\s*(-?\d+)>"#)
                .unwrap();
    }
    let caps = RE.captures(inp).unwrap();
    let result = Light {
        pos: Point {
            x: caps[1].parse().unwrap(),
            y: caps[2].parse().unwrap(),
        },
        spd: Point {
            x: caps[3].parse().unwrap(),
            y: caps[4].parse().unwrap(),
        },
    };
    result
}

/// Calculate the size of the boundingbox of the Map.
/// The size of the bouding box is the sum of height and width
fn bboxsz(map: &Map) -> i64 {
    let (tl, br) = bbox(map);
    (br.x - tl.x) + (br.y - tl.y) + 2
}

/// Calculate the bouding box of the give map.
/// Returns 2 Points: the top left point and the bottom right point
fn bbox(map: &Map) -> (Point, Point) {
    let tl: Point = map.iter().map(|lt| lt.pos).fold(
        Point {
            x: i64::MAX,
            y: i64::MAX,
        },
        |tl, p| Point {
            x: tl.x.min(p.x),
            y: tl.y.min(p.y),
        },
    );
    let br: Point = map.iter().map(|lt| lt.pos).fold(
        Point {
            x: i64::MIN,
            y: i64::MIN,
        },
        |tl, p| Point {
            x: tl.x.max(p.x),
            y: tl.y.max(p.y),
        },
    );
    // debug!("tl: ({},{}), br: ({},{})", tl.x, tl.y, br.x, br.y);
    (tl, br)
}

/// Derive a string that, when printed, represents the map
fn map2string(map: &Map) -> String {
    let (tl, br) = bbox(map);
    let szx = br.x - tl.x + 1;
    let szy = br.y - tl.y + 1;
    debug!("Map size: {} x {}", szx, szy);

    let mut strings: Vec<Vec<char>> = vec![vec!['.'; szx as usize]; szy as usize];
    for lt in map {
        strings[(lt.pos.y - tl.y) as usize][(lt.pos.x - tl.x) as usize] = '#';
    }
    strings
        .iter()
        .map(|line| line.iter().chain(['\n'].iter()).collect::<String>())
        .collect()
}

/// Return a new map with the positions adjusted to what they are after n steps.
fn pass_time(map: &Map, steps: usize) -> Map {
    map.iter()
        .map(|p| Light {
            pos: Point {
                x: p.pos.x + steps as i64 * p.spd.x,
                y: p.pos.y + steps as i64 * p.spd.y,
            },
            spd: p.spd,
        })
        .collect()
}

/// Given initial boundaries, a parameter and a function borrowingr the parameter find the n
/// where the function yields its minimum
///     let (min_map, min_n) = find_minimum(&map, 0, 1000000, | n |  bboxsz(&pass_time(&map, n )));
fn find_minimum<F>(map: &Map, a: usize, b: usize, f: F) -> usize
where
    F: Fn(usize) -> i64,
{
    let mut results: HashMap<usize, i64> = HashMap::new();
    let gr = (1.0 + (5_f64).sqrt()) / 2.0;
    let mut a = a;
    let mut b = b;
    loop {
        let fa = a as f64;
        let fb = b as f64;
        let c = (fb - (fb - fa) / gr).round() as usize;
        let d = (fa + (fb - fa) / gr).round() as usize;
        let fnc = f(c);
        let fnd = f(d);
        results.insert(c, fnc);
        results.insert(d, fnd);
        debug!(
            "Range: {} - {}. Trying {} -> {} and {} -> {}",
            a, b, c, fnc, d, fnd
        );
        if fnc < fnd {
            if b == d {
                break;
            }
            b = d;
        } else {
            if a == c {
                break;
            }
            a = c;
        }
    }
    let minv = results.values().min().unwrap();
    results
        .iter()
        .filter(|(&k, &v)| v == *minv)
        .map(|(k, v)| *k)
        .nth(0)
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    position=< 9,  1> velocity=< 0,  2>
    position=< 7,  0> velocity=<-1,  0>
    position=< 3, -2> velocity=<-1,  1>
    position=< 6, 10> velocity=<-2, -1>
    position=< 2, -4> velocity=< 2,  2>
    position=<-6, 10> velocity=< 2, -2>
    position=< 1,  8> velocity=< 1, -1>
    position=< 1,  7> velocity=< 1,  0>
    position=<-3, 11> velocity=< 1, -2>
    position=< 7,  6> velocity=<-1, -1>
    position=<-2,  3> velocity=< 1,  0>
    position=<-4,  3> velocity=< 2,  0>
    position=<10, -3> velocity=<-1,  1>
    position=< 5, 11> velocity=< 1, -2>
    position=< 4,  7> velocity=< 0, -1>
    position=< 8, -2> velocity=< 0,  1>
    position=<15,  0> velocity=<-2,  0>
    position=< 1,  6> velocity=< 1,  0>
    position=< 8,  9> velocity=< 0, -1>
    position=< 3,  3> velocity=<-1,  1>
    position=< 0,  5> velocity=< 0, -1>
    position=<-2,  2> velocity=< 2,  0>
    position=< 5, -2> velocity=< 1,  2>
    position=< 1,  4> velocity=< 2,  1>
    position=<-2,  7> velocity=< 2, -2>
    position=< 3,  6> velocity=<-1, -1>
    position=< 5,  0> velocity=< 1,  0>
    position=<-6,  0> velocity=< 2,  0>
    position=< 5,  9> velocity=< 1, -2>
    position=<14,  7> velocity=<-2,  0>
    position=<-3,  6> velocity=< 2, -1> 
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(1, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(1, part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
