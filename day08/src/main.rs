// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input08.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> i32 {
    let mut input = input[0]
        .split_whitespace()
        .map(|s| s.parse::<i32>().unwrap());
    let mut childs: Vec<i32> = vec![];
    let mut numdat: Vec<i32> = vec![];
    let mut state = States::GetNChilds;
    let mut sum = 0;
    loop {
        use States::*;
        match state {
            GetNChilds => {
                let num = input.next().unwrap();
                childs.push(num);
                state = States::GetNData;
            }
            GetNData => {
                let num = input.next().unwrap();
                numdat.push(num);
                state = if *childs.last().unwrap() == 0 {
                    GetData
                } else {
                    GetNChilds
                };
            }
            GetData => {
                let ndata = numdat.last_mut().unwrap();
                while *ndata > 0 {
                    sum += input.next().unwrap();
                    *ndata -= 1;
                }
                childs.pop();
                numdat.pop();
                if childs.is_empty() {
                    break;
                }
                let nchilds = childs.last_mut().unwrap();
                if *nchilds > 1 {
                    *nchilds -= 1;
                    state = GetNChilds;
                } else {
                    state = GetData;
                }
            }
        }
    }
    sum
}

fn part2(input: &[&str]) -> i32 {
    let mut input = input[0]
        .split_whitespace()
        .map(|s| s.parse::<i32>().unwrap());
    let mut nodes: Vec<Node> = vec![];
    let mut state = States::GetNChilds;
    loop {
        debug!("{:?}", nodes);
        use States::*;
        match state {
            GetNChilds => {
                let num = input.next().unwrap();
                nodes.push(Node {
                    nchild: num,
                    ndata: 0,
                    values: vec![],
                });
                state = States::GetNData;
            }
            GetNData => {
                let num = input.next().unwrap();
                let node = nodes.last_mut().unwrap();
                node.ndata = num;
                state = if node.nchild == 0 {
                    GetData
                } else {
                    GetNChilds
                };
            }
            GetData => {
                let mut value = 0;
                {
                    let node = nodes.last().unwrap();
                    if node.nchild != 0 {
                        for _ in 0..node.ndata {
                            let dt = input.next().unwrap();
                            value += node.values.get(0.max(dt - 1) as usize).unwrap_or(&0);
                        }
                    } else {
                        for _ in 0..node.ndata {
                            value += input.next().unwrap();
                        }
                    }
                } // borrow from nodes ends here. Value still exists.
                nodes.pop();
                if nodes.is_empty() {
                    break value;
                }
                let node = nodes.last_mut().unwrap();
                node.values.push(value);
                if node.nchild > 1 {
                    node.nchild -= 1;
                    state = GetNChilds;
                } else {
                    state = GetData;
                }
            }
        }
    }
}

#[derive(Debug)]
struct Node {
    nchild: i32,
    ndata: i32,
    values: Vec<i32>, // Stores the values of the childs until the own data is read
}

enum States {
    GetNChilds,
    GetNData,
    GetData,
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(138, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(66, part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
