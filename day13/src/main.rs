// Needed use statements for scaffolding
use std::fmt::{self, Debug, Display};
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input13.txt");

// const LOGLEVEL: LevelFilter = LevelFilter::Info;
const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Debug>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {:?} in {elapsed} ms", result);
}

fn part1(input: &[&str]) -> (usize, usize) {
    let mut map = Map::new(input);
    // debug!(
    //     "Map size (wxh): {} x {},  {} carts",
    //     map.sz.0,
    //     map.sz.1,
    //     map.carts.len()
    // );
    // debug!("\n{}", map);
    let mut ticks = 0;
    loop {
        if let Err(pt) = map.tick() {
            // debug!("Collision after tick {} at ({},{})", ticks + 1, pt.x, pt.y);
            // debug!("\n{}", map);
            // for cart in map.carts {
            //     let trajstr = cart
            //         .traj
            //         .iter()
            //         .map(|p| format!("{}", p))
            //         .collect::<Vec<String>>();
            //     // debug!(
            //     //     "cart {} @{} to {}: {}",
            //     //     cart.id,
            //     //     cart.pos,
            //     //     cart.dir,
            //     //     &trajstr[..].join(" ")
            //     // );
            // }
            return (pt.x, pt.y);
        }
        ticks += 1;
        // debug!("After tick {}", ticks);
        // for cart in &map.carts {
        //     debug!("{:?}", cart)
        // }
        // debug!("\n{}", map);
        // if ticks > 20 {
        //     break;
        // }
    }
}

fn part2(input: &[&str]) -> (usize, usize) {
    let mut map = Map::new(input);
    debug!(
        "Map size (wxh): {} x {},  {} carts",
        map.sz.0,
        map.sz.1,
        map.carts.len()
    );
    let mut ticks = 0;
    while map.carts.len() > 1 {
        map.tick2();
        ticks += 1;
        // if ticks > 20 {
        //     panic!("Too many ticks2");
        // }
    }
    println!("Result after {ticks} ticks");
    let p = map.carts[0].pos;
    return (p.x, p.y);
}

/// Map encodes the complete input.
///    tracks is a 2D array of track pieces.
///    carts is vector of points where carts are located.

#[derive(Debug)]
struct Map {
    tracks: Vec<Vec<TrkPc>>,
    carts: Vec<Cart>,
    sz: (usize, usize),
}

/// A TrkPc (track piece) connects trackpieces in 2 or 4 directions
/// A Cross(ing) connects 4 directions
/// A Conn(ection) connects 2 directions.
/// A Conn holds the directions it connects, the lowest valued first
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum TrkPc {
    Cross,
    Conn(Dir, Dir),
    None,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Dir {
    N = 0, // North
    E,     // East
    S,     // South
    W,     // West
}

/// A Cart is defined by
/// - its position on the tracks
/// - the position it is heading to
/// - the turn it will make on the next Cross
#[derive(Debug, Clone)]
struct Cart {
    id: u8,
    pos: Point,
    dir: Dir,
    nxt_turn: Turn,
    traj: Vec<Point>,
}

const CART_CH: [char; 4] = ['^', 'v', '<', '>'];
const TRACK_CH: [char; 4] = ['|', '|', '-', '-'];

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Clone, Copy)]
struct Point {
    x: usize,
    y: usize,
}

// A turn denotes the direction change a cart an make on a Cross
#[derive(Debug, Clone, Copy)]
enum Turn {
    L = 0, // Left
    S,     // Straight
    R,     // Right
}

impl Map {
    /// Takes a set of str and distills a map of tracks and carts from it
    fn new(input: &[&str]) -> Self {
        let ht = input.len();
        let wd = input.iter().map(|line| line.len()).max().unwrap();
        let mut tracks = Vec::with_capacity(ht);
        let mut carts: Vec<Cart> = vec![];
        for (y, line) in input.iter().enumerate() {
            let mut prv = TrkPc::None;
            let mut track_y: Vec<TrkPc> = vec![TrkPc::None; wd];
            for (x, c) in line.chars().enumerate() {
                let mut c = c;
                if let Some(pos) = CART_CH.iter().position(|&ch| ch == c) {
                    carts.push(Cart::new(carts.len() as u8, x, y, c).unwrap());
                    c = TRACK_CH[pos];
                }
                let trkpc = TrkPc::fromch(c, &prv);
                track_y[x] = trkpc;
                prv = trkpc;
            }
            tracks.push(track_y);
        }
        Map {
            tracks,
            carts,
            sz: (wd, ht),
        }
    }

    /// tick() advances all carts 1 position.
    /// First the carts are sorted accrding to y, then x
    /// Then each cart is moved one step.
    /// The return value is the location of the first collision, if any.
    /// On a collision, tick() is immediately aborted
    fn tick(&mut self) -> Result<(), Point> {
        self.carts.sort_by_key(|c| Point {
            x: c.pos.y,
            y: c.pos.x,
        });
        let mut new_carts: Vec<Cart> = Vec::with_capacity(self.carts.len());
        for cart in self.carts.iter() {
            let new_cart = self.move_cart(cart);
            if new_carts
                .iter()
                .chain(self.carts[self.carts.len().min(new_carts.len() + 1)..].iter())
                .map(|c| c.pos)
                .collect::<Vec<_>>()
                .contains(&new_cart.pos)
            {
                return Err(new_cart.pos);
            }
            new_carts.push(new_cart);
        }
        self.carts = new_carts;
        Ok(())
    }

    /// tick2() advances all carts 1 position.
    /// First the carts are sorted accrding to y, then x
    /// Then each cart is moved one step.
    /// If 2 carts collide, they are taken out
    fn tick2(&mut self) {
        self.carts.sort_by_key(|c| Point {
            x: c.pos.y,
            y: c.pos.x,
        });

        let mut collision_idx: Vec<usize> = vec![]; // Collection of carts that suffered a collision before moving
        let mut new_carts: Vec<Cart> = Vec::with_capacity(self.carts.len()); // Collection of moved carts

        // Try moving alll carts. Moved carts are collected in new_carts
        for (n, cart) in self.carts.iter().enumerate() {
            if collision_idx.contains(&n) {
                continue; // This cart has been collided on. Do not move to new_carts
            }
            let new_cart = self.move_cart(cart);
            if let Some(idx) = new_carts.iter().position(|c| c.pos == new_cart.pos) {
                // New cart collided with already moved cart.
                new_carts.remove(idx); // So remove already moved cart
                continue; // And do not make this cart a new cart
            }
            if let Some(idx) = self.carts[n + 1..]
                .iter()
                .position(|c| c.pos == new_cart.pos)
            {
                // New cart collided with not yet moved cart. Store its index
                collision_idx.push(idx + n + 1);
                continue; // And do not make this cart a new cart
            }
            new_carts.push(new_cart);
        }
        self.carts = new_carts;
    }

    /// move_cart() moves 1 cart 1 step, and returns a new cart.
    /// The dir of the new cart takes all forced and self-driven turns into account, so before that the move is simply
    /// in the carts direction, after which the turning is handled.
    fn move_cart(&self, cart: &Cart) -> Cart {
        use Dir::*;
        use TrkPc::*;
        let mut new_cart = cart.clone();
        match cart.dir {
            N => new_cart.pos.y -= 1,
            E => new_cart.pos.x += 1,
            S => new_cart.pos.y += 1,
            W => new_cart.pos.x -= 1,
        }
        let trkpc = self.tracks[new_cart.pos.y][new_cart.pos.x];
        match trkpc {
            Conn(N, S) if cart.dir == N || cart.dir == S => (),
            Conn(E, W) if cart.dir == W || cart.dir == E => (),
            Conn(N, E) if cart.dir == S => new_cart.dir = E,
            Conn(N, E) if cart.dir == W => new_cart.dir = N,
            Conn(N, W) if cart.dir == S => new_cart.dir = W,
            Conn(N, W) if cart.dir == E => new_cart.dir = N,
            Conn(E, S) if cart.dir == W => new_cart.dir = S,
            Conn(E, S) if cart.dir == N => new_cart.dir = E,
            Conn(S, W) if cart.dir == E => new_cart.dir = S,
            Conn(S, W) if cart.dir == N => new_cart.dir = W,
            Cross => {
                let new_dir = Dir::from(cart.dir as u8 + cart.nxt_turn as u8 + 3);
                let new_turn = Turn::from(cart.nxt_turn as u8 + 1);
                new_cart.dir = new_dir;
                new_cart.nxt_turn = new_turn;
            }
            _ => unreachable!(
                "I should not get, but got {:?}, while going {:?} @ {:?}",
                trkpc, cart.dir, new_cart.pos
            ),
        };
        new_cart.traj.push(new_cart.pos.clone());
        new_cart
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut result: Vec<String> = Vec::with_capacity(self.sz.1);
        for y in 0..self.sz.1 {
            let mut line = String::with_capacity(self.sz.0);
            for (x, trkpc) in self.tracks[y].iter().enumerate() {
                if let Some(crtpos) = self.carts.iter().position(|c| c.pos == Point { x, y }) {
                    line.push(self.carts[crtpos].as_char());
                } else {
                    line.push(trkpc.as_char());
                }
            }
            result.push(line);
        }
        write!(f, "{}", result.join("\n"))
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl TrkPc {
    /// Convert a character into a track piece.
    /// For the / and \ characters the previous track piece is needed to select the right one
    fn fromch(c: char, prv: &TrkPc) -> Self {
        use Dir::*;
        use TrkPc::*;
        let east_set = [Conn(E, W), Conn(N, E), Conn(E, S), Cross]; // Set of connections with an exit to the East
        let result = match c {
            '|' => Conn(N, S),
            '-' if east_set.contains(prv) => Conn(E, W),
            '/' if east_set.contains(prv) => Conn(N, W),
            '/' => Conn(E, S),
            '\\' if east_set.contains(prv) => Conn(S, W),
            '\\' => Conn(N, E),
            '+' if east_set.contains(prv) => Cross,
            _ => None,
        };
        trace!("TrkPc::frmch got {:x}: '{}' -> {:?}", c as u8, c, result);
        result
    }

    fn as_char(&self) -> char {
        use Dir::*;
        use TrkPc::*;
        match self {
            None => ' ',
            Cross => '+',
            Conn(N, S) => '|',
            Conn(E, W) => '-',
            Conn(N, E) | Conn(S, W) => '\\',
            Conn(N, W) | Conn(E, S) => '/',
            _ => '#',
        }
    }
}

impl From<u8> for Dir {
    fn from(nr: u8) -> Self {
        Dir::ARR[nr as usize % Dir::ARR.len()]
    }
}

impl Display for Dir {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Dir::N => 'N',
                Dir::E => 'E',
                Dir::S => 'S',
                Dir::W => 'W',
            }
        )
    }
}

impl Dir {
    const ARR: [Dir; 4] = [Dir::N, Dir::E, Dir::S, Dir::W];
}

impl From<u8> for Turn {
    fn from(nr: u8) -> Self {
        Turn::ARR[nr as usize % Turn::ARR.len()]
    }
}

impl Turn {
    const ARR: [Turn; 3] = [Turn::L, Turn::S, Turn::R];
}

impl Cart {
    /// Create a new cart with position (x, y) pointing in a direction according to c
    /// if c is not one of < > v ^  None is returned
    fn new(id: u8, x: usize, y: usize, c: char) -> Option<Cart> {
        let dir = match c {
            '<' => Dir::W,
            '>' => Dir::E,
            '^' => Dir::N,
            'v' => Dir::S,
            _ => return None,
        };
        Some(Cart {
            id,
            pos: Point { x, y },
            dir,
            nxt_turn: Turn::L,
            traj: vec![Point { x, y }],
        })
    }

    fn as_char(&self) -> char {
        use Dir::*;
        match self.dir {
            N => '^',
            E => '>',
            S => 'v',
            W => '<',
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = r"\
/->-\        
|   |  /----\
| /-+--+-\  |
| | |  | v  |
\-+-/  \-+--/
  \------/";

    const INPUT02: &str = r"\
/>-<\  
|   |  
| /<+-\
| | | v
\>+</ |
  |   ^
  \<->/";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().skip(1).filter(|s| !s.is_empty()).collect();
        assert_eq!((7, 3), part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT02.lines().skip(1).filter(|s| !s.is_empty()).collect();
        assert_eq!((6, 4), part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
