// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::VecDeque; //{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = "411 players; last marble is worth 71058 points";

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> u64 {
    let (players, hi_marble) = get_game_parameters(input[0]);
    debug!(
        "Playing with {} players and marbles until {} points",
        players, hi_marble
    );
    let result = play_game2(players, hi_marble);
    debug!("---> {}", result);
    result
}

fn part2(input: &[&str]) -> u64 {
    let (players, hi_marble) = get_game_parameters(input[0]);
    debug!(
        "Playing with {} players and marbles until {} points",
        players,
        100 * hi_marble
    );
    let result = play_game2(players, 100 * hi_marble);
    debug!("---> {}", result);
    result
}

type Marble = u64;

fn get_game_parameters(inp: &str) -> (usize, u64) {
    let words: Vec<_> = inp.split_whitespace().collect();
    let players = words[0].parse().unwrap();
    let hi_marble = words[6].parse().unwrap();
    (players, hi_marble)
}

/// This version takes 4.3 hours(!) to complete part 2 in  release version
fn play_game(players: usize, hi_marble: Marble) -> u64 {
    let mut circle: Vec<Marble> = Vec::with_capacity(hi_marble as usize);
    let mut score: Vec<u64> = vec![0; players];
    circle.push(0);
    circle.push(2);
    circle.push(1);
    let mut current: usize = 1;
    let mut player = 2;
    for marble in 3..=hi_marble {
        if marble % 100_000 == 0 {
            print!("     Marble {}\r", marble);
            std::io::stdout().flush().unwrap();
        }
        if marble % 23 != 0 {
            current += 2;
            if current > circle.len() {
                current -= circle.len()
            }
            circle.insert(current, marble);
        } else {
            if current < 7 {
                current += circle.len();
            }
            current -= 7;
            let removed = circle.remove(current);
            score[player] += marble + removed;
        }
        player += 1;
        if player >= players {
            player = 0;
        }
    }
    *score.iter().max().unwrap()
}

/// This version uses a rotating VecDeque, saving many many O(n) shift operations
/// Takes 0.112 seconds to complete part 2, i.o. 4.3 hrs
fn play_game2(players: usize, hi_marble: Marble) -> u64 {
    let mut circle: VecDeque<Marble> = VecDeque::with_capacity(hi_marble as usize);
    let mut score: Vec<u64> = vec![0; players];
    circle.push_back(0);
    circle.push_back(2);
    circle.push_back(1);
    circle.rotate_left(1);
    let mut player = 2;
    for marble in 3..=hi_marble {
        if marble % 23 != 0 {
            circle.rotate_left(2);
            circle.push_front(marble);
        } else {
            circle.rotate_right(7);
            let removed = circle.pop_front().unwrap();
            score[player] += marble + removed;
        }
        player += 1;
        if player >= players {
            player = 0;
        }
    }
    *score.iter().max().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    9 players; last marble is worth 25 points
    10 players; last marble is worth 1618 points
    13 players; last marble is worth 7999 points
    17 players; last marble is worth 1104 points
    21 players; last marble is worth 6111 points
    30 players; last marble is worth 5807 points
";

    const WANT01: &[u64] = &[32, 8317, 146373, 2764, 54718, 37305];

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        for (gamenr, (inp, want)) in input.iter().zip(WANT01.iter()).enumerate() {
            debug!("{}: '{}' -> {}", gamenr, inp, want);
            assert_eq!(*want, part1(&[inp]));
        }
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
