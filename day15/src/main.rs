// Needed use statements for scaffolding
use std::fmt::{self, Display};
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, warn, Level, LevelFilter};

use std::collections::{HashSet, VecDeque}; //{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input15.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> i64 {
    let mut arena = Arena::new(input);
    debug!("\n{}", arena.to_string());
    debug!("szx: {}, szy: {}", arena.szx, arena.szy);
    let turns = arena.combat();
    let mapscore = arena.score1();
    let result = turns * mapscore;
    debug!("Map score: {}, turns: {}", mapscore, turns);
    debug!("Final score: {result}");
    debug!("\n{}", arena.to_string());
    debug!("");
    result
}

fn part2(input: &[&str]) -> i64 {
    let arena = Arena::new(input);
    let start_elves = arena.elves.clone();
    let elves0 = start_elves.len();
    let mut min_power = 4; // Less is known to be too little
    let mut max_power = i32::MAX; // This value has been tried and is OK
    let mut score_at_max = 0;
    let mut attack_power = 4;
    let mut cnt = 0;
    loop {
        let mut arena = Arena::new(input);
        arena
            .elves
            .iter_mut()
            .for_each(|e| e.attack_pwr = attack_power);
        debug!("Attack power: {}", attack_power);
        let turns = arena.combat2();
        let (nume, numg, sume, sumg) = arena.score2();
        debug!(
            "After {} turns with attack power {}: elves: {}, goblins: {}, sum_elves: {}, sum_goblins: {}",
            turns, attack_power, nume, numg, sume, sumg
        );
        if nume < elves0 {
            // Need more attack power, more then min_power and current attack_power but less then the current max_power
            let highg = arena.goblins.iter().map(|s| s.hit_pnts).max().unwrap();
            min_power = max_power.min(attack_power + 1);
            if cnt == 0 {
                attack_power += highg / turns as i32;
            } else {
                attack_power =
                    min_power.max((max_power - 1).min(attack_power + highg / turns as i32));
            }
        } else {
            // Maybe need less attack_power,  but at least min_power
            max_power = attack_power;
            score_at_max = turns * arena.score1();
            attack_power = min_power.max(if cnt == 0 {
                2 * min_power
            } else {
                (attack_power - min_power) / 2
            });
        }
        debug!("minpower: {min_power} / maxpower: {max_power}");
        if max_power <= min_power {
            debug!("Elves need {min_power} to survive in {turns} turns, ending with score {} for {score_at_max} points", arena.score1());
            break score_at_max;
        }
        cnt += 1;
        if cnt > 8 {
            break 0;
        }
    }
}

type Map = Vec<Vec<Loc>>;

struct Arena {
    map: Map,
    elves: Vec<Specimen>,
    goblins: Vec<Specimen>,
    szx: usize,
    szy: usize,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Loc {
    Floor,
    Elf(usize),  // Index into the Arena's elves
    Gobl(usize), // Index into the Arena's goblins
    Wall,
    Huh, // ??? Unknown symbol
}

// #[derive(Debug, Clone, Copy, PartialEq)]
// struct FightData {
//     attack_pwr: i32, // hitpoints loss of opponent per attack
//     hit_pnts: i32,   // Need at least 1 to live
// }

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Point {
    x: usize,
    y: usize,
}

/// Specimen is used to hold the data of an Elf or Goblin
#[derive(Debug, Clone, Copy, PartialEq)]
struct Specimen {
    id: usize,
    pnt: Point,
    attack_pwr: i32,
    hit_pnts: i32,
}

impl Arena {
    /// new() creates an Arena from a vector of strings
    fn new(input: &[&str]) -> Arena {
        let mut map = vec![];
        let mut elves = vec![];
        let mut goblins = vec![];
        for (y, &ystr) in input.iter().enumerate() {
            let mut xlist = vec![];
            for (x, ch) in ystr.chars().enumerate() {
                let mut loc = Loc::from(ch);
                match loc {
                    Loc::Elf(ref mut id) => {
                        *id = elves.len();
                        elves.push(Specimen {
                            id: elves.len(),
                            pnt: Point { x, y },
                            attack_pwr: 3,
                            hit_pnts: 200,
                        })
                    }
                    Loc::Gobl(ref mut id) => {
                        *id = goblins.len();
                        goblins.push(Specimen {
                            id: goblins.len(),
                            pnt: Point { x, y },
                            attack_pwr: 3,
                            hit_pnts: 200,
                        })
                    }
                    _ => (),
                }
                xlist.push(loc);
            }
            map.push(xlist);
        }
        let szx = map[0].len();
        let szy = map.len();
        Arena {
            map,
            elves,
            goblins,
            szx,
            szy,
        }
    }

    /// map_to_string converts the map into a multiline string for printing
    fn to_string(&self) -> String {
        let mut ystrv: Vec<String> = vec![];
        for yvec in &self.map {
            ystrv.push(yvec.iter().map(|loc| char::from(loc)).collect());
        }
        ystrv.join("\n")
    }

    /// combat() changes the map while the species are combatting,
    /// until only 1 species is left
    /// Returns the number of combat rounds conducted to wipe out 1 species
    fn combat(&mut self) -> i64 {
        let mut cnt = 0;
        loop {
            cnt += 1;
            debug!("Starting round {}", cnt);
            if let Some(round_at_end) = self.combat1turn(false) {
                if round_at_end {
                    cnt += 1
                }
                debug!(
                    "After {} full rounds, combat ends in round {}:\n{}",
                    cnt - 1,
                    cnt,
                    self.to_string()
                );
                break cnt - 1;
            }
            debug!("After {} rounds:\n{}", cnt, self.to_string());
            self.score1();
        }
    }

    /// combat2() performs combatting until all goblins are dead or the 1st elve dies
    /// Returns the number of combat rounds completed before this hapens
    fn combat2(&mut self) -> i64 {
        let mut cnt = 0;
        loop {
            cnt += 1;
            debug!("Starting round {}", cnt);
            if let Some(round_at_end) = self.combat1turn(true) {
                if round_at_end {
                    cnt += 1
                }
                debug!(
                    "After {} full rounds, combat ends in round {}:\n{}",
                    cnt - 1,
                    cnt,
                    self.to_string()
                );
                break cnt - 1;
            }
            debug!("After {} rounds:\n{}", cnt, self.to_string());
            self.score1();
        }
    }

    /// Do 1 combat turn for all specimen
    /// Returns true if one species is extinct
    fn combat1turn(&mut self, stop_at_1st_elf: bool) -> Option<bool> {
        let mut fightorder: Vec<Point> = vec![];
        for y in 0..self.map.len() {
            for x in 0..self.map[y].len() {
                if let Loc::Elf(_) | Loc::Gobl(_) = &self.map[y][x] {
                    fightorder.push(Point { x, y });
                }
            }
        }

        debug!(
            "Combat starts with {} elves and {} goblins",
            self.elves.iter().filter(|e| e.hit_pnts > 0).count(),
            self.goblins.iter().filter(|g| g.hit_pnts > 0).count(),
        );

        for n in 0..fightorder.len() {
            // Get all the data at hand
            let mut joe_pnt = fightorder[n];
            let joe = self.map[joe_pnt.y][joe_pnt.x];
            let joe_id = if let Loc::Elf(id) | Loc::Gobl(id) = joe {
                id
            } else {
                // A fighter died and is no more.
                continue;
            };
            // move
            if let Some(newpnt) = self.do_move(joe_pnt, joe) {
                if newpnt != joe_pnt {
                    self.map[joe_pnt.y][joe_pnt.x] = Loc::Floor;
                    self.map[newpnt.y][newpnt.x] = joe;
                    debug!("Joe @ {} moving to {} ", joe_pnt, newpnt);
                    joe_pnt = newpnt;
                    match joe {
                        Loc::Elf(_) => self.elves[joe_id].pnt = newpnt,
                        Loc::Gobl(_) => self.goblins[joe_id].pnt = newpnt,
                        _ => unreachable!(),
                    }
                } else {
                    debug!("Joe @ {} stays on its spot", joe_pnt);
                }
            }

            // attack
            let joe_data = if joe.is_elf() {
                &self.elves[joe_id]
            } else {
                &self.goblins[joe_id]
            };

            let nbrs = joe_pnt.adjacent(self.szx, self.szy);
            let min_points = nbrs
                .iter()
                .filter_map(|p| match self.map[p.y][p.x] {
                    Loc::Gobl(id) if joe.is_elf() => Some(self.goblins[id].hit_pnts),
                    Loc::Elf(id) if joe.is_goblin() => Some(self.elves[id].hit_pnts),
                    _ => None,
                })
                .min(); // Least hit points of possible victims, or no victims around
            if let Some(minp) = min_points {
                // We have at least 1 victim: joe can attack
                let force = joe_data.attack_pwr;
                let victim_pnt = nbrs
                    .iter()
                    .filter(|&p| match self.map[p.y][p.x] {
                        Loc::Gobl(id) if joe.is_elf() => self.goblins[id].hit_pnts == minp,
                        Loc::Elf(id) if joe.is_goblin() => self.elves[id].hit_pnts == minp,
                        _ => false,
                    })
                    .map(|p| *p)
                    .collect::<Vec<Point>>()[0];
                debug!("Joe attacks victim @ {}", victim_pnt);
                let victim = &mut self.map[victim_pnt.y][victim_pnt.x];
                let is_elf = victim.is_elf();
                let v_str = if is_elf { "Elf" } else { "Goblin" };
                match victim {
                    Loc::Elf(id) | Loc::Gobl(id) => {
                        let victim_data = if is_elf {
                            &mut self.elves[*id]
                        } else {
                            &mut self.goblins[*id]
                        };
                        victim_data.hit_pnts -= force;
                        debug!(
                            "{} @ {} now has {} hit points",
                            v_str, victim_pnt, victim_data.hit_pnts
                        );
                        if victim_data.hit_pnts <= 0 {
                            debug!("{} @ {} died!", v_str, victim_pnt);
                            *victim = Loc::Floor;
                        }
                    }
                    _ => unreachable!(),
                }
            }
            if self.goblins.iter().all(|g| g.hit_pnts <= 0)
                || (!stop_at_1st_elf && self.elves.iter().all(|e| e.hit_pnts <= 0))
                || (stop_at_1st_elf && self.elves.iter().any(|e| e.hit_pnts <= 0))
            {
                return if n == fightorder.len() - 1 {
                    return Some(true); // Count this round as completed
                } else {
                    Some(false)
                };
            }
        }
        None // Fight continues
    }

    /// score1 determines a map's score according to the rules of part1 of day 15
    /// Only 1 species should be left
    fn score1(&self) -> i64 {
        let mut score = 0;
        debug!("Scores:");
        for row in &self.map {
            score += row
                .iter()
                .map(|loc| match loc {
                    Loc::Elf(id) => {
                        let hits = self.elves[*id].hit_pnts;
                        debug!("E({})", hits);
                        hits
                    }
                    Loc::Gobl(id) => {
                        let hits = self.goblins[*id].hit_pnts;
                        debug!("G({})", hits);
                        hits
                    }
                    _ => 0,
                })
                .sum::<i32>();
        }
        score as i64
    }

    // score2 returns num_elves, nun_goblins, elve hitpoints left, goblins hitpoints left
    fn score2(&self) -> (usize, usize, i32, i32) {
        let num_elves = self.elves.iter().filter(|e| e.hit_pnts > 0).count();
        let num_goblins = self.goblins.iter().filter(|g| g.hit_pnts > 0).count();
        let sum_elves: i32 = self
            .elves
            .iter()
            .filter(|e| e.hit_pnts > 0)
            .map(|e| e.hit_pnts)
            .sum();
        let sum_goblins: i32 = self
            .goblins
            .iter()
            .filter(|g| g.hit_pnts > 0)
            .map(|g| g.hit_pnts)
            .sum();
        (num_elves, num_goblins, sum_elves, sum_goblins)
    }
    /// do_move returns takes the map, the sorted fighterslist in reading order and the index of the
    /// current fighter in the fighter list, named joe
    /// It selects a target, a path, and does 1 move if needed.
    /// The new coordinates are returned.
    /// The caller should actually move the specimen
    fn do_move(&self, pnt: Point, joe: Loc) -> Option<Point> {
        debug!(
            "Attempt to move {} @ {}",
            if joe.is_elf() { "Elf" } else { "Goblin" },
            pnt
        );

        // See if we are already adjacent to a target
        let nbrs = pnt.adjacent(self.szx, self.szy);
        for nbr in nbrs {
            let next_to_target = match self.map[nbr.y][nbr.x] {
                Loc::Elf(_) if joe.is_goblin() => true,
                Loc::Gobl(_) if joe.is_elf() => true,
                _ => false,
            };
            if next_to_target {
                debug!("Next to target");
                return Some(pnt);
            }
        }

        // Select target combattants
        let targets = if joe.is_elf() {
            &self.goblins
        } else {
            &self.elves
        };

        // Find open spots next to targets
        let mut dests: HashSet<Point> = HashSet::with_capacity(4 * targets.len()); // HashSet removes doubles
        for trgt in targets {
            if trgt.hit_pnts <= 0 {
                continue;
            }
            let pnts = trgt.pnt.adjacent(self.szx, self.szy);
            dests.extend(
                pnts.into_iter()
                    .filter(|p| Loc::Floor == self.map[p.y][p.x]),
            );
        }
        debug!(
            "Open spots: {}",
            dests.iter().map(|p| p.to_string()).collect::<String>()
        );
        // Find nearest open spots.
        self.find_nearest(pnt, dests)
    }

    /// find_nearest() finds all points in dests on the smallest distance to any point in dests
    /// The result is returned in reading order
    /// IF you are standing on a destination point, this  point will be the only one in the return vector
    fn find_nearest(&self, start: Point, dests: HashSet<Point>) -> Option<Point> {
        struct State {
            p: Point,       // point
            d: usize,       // distance to start
            tr: Vec<Point>, // Trajectory up to this point
        }
        let mut result: Vec<State> = vec![];
        let mut queue: VecDeque<State> = VecDeque::new();
        let mut visited: HashSet<Point> = HashSet::new();
        let mut distance = usize::MAX;
        queue.push_back(State {
            p: start,
            d: 0,
            tr: vec![],
        });
        while let Some(state) = queue.pop_front() {
            // debug!(
            //     "Looking at {}, distance {} / {}",
            //     state.p, state.d, distance
            // );
            if state.d > distance {
                continue; // Point not interesting: too far away
            }
            if dests.contains(&state.p) {
                if state.d < distance {
                    distance = state.d; // Record smallest distance
                }
                // debug!("Result += {}", state.p);
                result.push(state); // first point or state.d == distance: Target point at same distance
                continue; // Subsequent points would be too far
            }
            if state.d == distance {
                continue; // Any subsequent point of a non-target point at distance would be too far
            }
            if visited.contains(&state.p) {
                continue; // No need to double back to already seen points
            }
            visited.insert(state.p);
            // Over here we have a point where we want to search further away
            let mut tr = state.tr;
            tr.push(state.p); // Prepare trajectory for subsequent points
            let pnts = state.p.adjacent(self.map[state.p.y].len(), self.map.len());
            queue.extend(
                pnts.into_iter()
                    .filter(|p| Loc::Floor == self.map[p.y][p.x])
                    .map(|p| State {
                        p: p,
                        d: state.d + 1,
                        tr: tr.clone(),
                    }),
            );
        }
        // debug!("1: {:?}", result.iter().map(|st| st.p).collect::<Vec<_>>());
        // result.sort_by_key(|st| (st.p.y, st.p.x));
        // debug!("2: {:?}", result.iter().map(|st| st.p).collect::<Vec<_>>());
        // if let Some(st) = result.get(0) {
        //     debug!("3: {:?}", result[0].tr);
        // }
        if result.is_empty() {
            // Cannot reach any destination
            None
        } else if result[0].tr.len() <= 1 {
            // The state point shows the first and final step
            Some(result[0].p)
        } else {
            // First step towards destination
            Some(result[0].tr[1])
        }
    }
}

impl Loc {
    fn is_elf(&self) -> bool {
        if let Loc::Elf(_) = self {
            true
        } else {
            false
        }
    }

    fn is_goblin(&self) -> bool {
        if let Loc::Gobl(_) = self {
            true
        } else {
            false
        }
    }
}

impl Point {
    /// find adjacent points to self
    /// Adjacent points cannot have indices < 0 or >= szx or szy
    /// They are returned in "reading order":  N, W, E, S
    fn adjacent(&self, szx: usize, szy: usize) -> Vec<Point> {
        let mut result = Vec::with_capacity(4);
        let x = self.x;
        let y = self.y;
        if y > 1 {
            result.push(Point { x, y: y - 1 });
        }
        if x > 1 {
            result.push(Point { x: x - 1, y });
        }
        if x + 1 < szx {
            result.push(Point { x: x + 1, y });
        }
        if y + 1 < szy {
            result.push(Point { x, y: y + 1 });
        }
        result
    }
}

impl From<char> for Loc {
    fn from(inp: char) -> Self {
        match inp {
            '.' => Loc::Floor,
            'E' => Loc::Elf(0),
            'G' => Loc::Gobl(0),
            '#' => Loc::Wall,
            _ => {
                warn!("Unexpected symbol '{inp}' for Loc");
                Loc::Huh
            }
        }
    }
}

impl From<&Loc> for char {
    fn from(inp: &Loc) -> char {
        match inp {
            Loc::Floor => '.',
            Loc::Wall => '#',
            Loc::Elf(_) => 'E',
            Loc::Gobl(_) => 'G',
            Loc::Huh => '?',
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01A: &str = "\
    #######   
    #.G...#
    #...EG#
    #.#.#G#
    #..G#E#
    #.....#   
    #######
";

    const INPUT01B: &str = "\
    #######
    #G..#E#
    #E#E.E#
    #G.##.#
    #...#E#
    #...E.#
    #######
";

    const INPUT01C: &str = "\
    #######
    #E..EG#
    #.#G.E#
    #E.##E#
    #G..#.#
    #..E#.#
    #######
";

    const INPUT01D: &str = "\
    #######
    #E.G#.#
    #.#G..#
    #G.#.G#
    #G..#.#
    #...E.#
    #######
";

    const INPUT01E: &str = "\
    #######
    #.E...#
    #.#..G#
    #.###.#
    #E#G#G#
    #...#G#
    #######
";

    const INPUT01F: &str = "\
    #########
    #G......#
    #.E.#...#
    #..##..G#
    #...##..#
    #...#...#
    #.G...G.#
    #.....G.#
    #########
";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        assert_eq!(27730, do_test1(INPUT01A));
        assert_eq!(36334, do_test1(INPUT01B));
        assert_eq!(39514, do_test1(INPUT01C));
        assert_eq!(27755, do_test1(INPUT01D));
        assert_eq!(28944, do_test1(INPUT01E));
        assert_eq!(18740, do_test1(INPUT01F));
    }

    fn do_test1(inp: &str) -> i64 {
        let input: Vec<_> = inp
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        part1(&input)
    }

    #[test]
    fn test2() {
        init_logger();
        assert_eq!(4988, do_test2(INPUT01A));
        assert_eq!(31284, do_test2(INPUT01C));
        assert_eq!(3478, do_test2(INPUT01D));
        assert_eq!(6474, do_test2(INPUT01E));
        assert_eq!(1140, do_test2(INPUT01F));
    }

    fn do_test2(inp: &str) -> i64 {
        let input: Vec<_> = inp
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        part2(&input)
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
