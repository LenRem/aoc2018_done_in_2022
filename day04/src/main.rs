// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::HashMap; //{HashSet, HashMap, VecDeque};
use std::ops::Range;

use lazy_static::lazy_static;
use regex::Regex;

const INPUTTXT: &str = include_str!("../input04.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let mut events: Events = input.iter().map(|&s| Event::from(s)).collect();
    events.sort();
    let sleep_ranges = find_sleep_ranges(&events);
    let guard = find_sleepiest(&sleep_ranges);
    let (mnt, _) = find_sleep_minute(&sleep_ranges[&guard]);
    info!("Guard {} sleeps the most in minute {}", guard, mnt);
    guard * mnt as usize
}

fn part2(input: &[&str]) -> usize {
    let mut events: Events = input.iter().map(|&s| Event::from(s)).collect();
    events.sort();
    let sleep_ranges = find_sleep_ranges(&events);
    let popular_minutes: HashMap<_, _> = sleep_ranges
        .iter()
        .map(|(grd, ranges)| {
            let (mnt, cnt) = find_sleep_minute(ranges);
            (*grd, (mnt, cnt))
        })
        .collect();
    // This iterator might be glued after the previous one, for showing off.
    let result = popular_minutes
        .iter()
        .reduce(|r, e| if e.1 .1 > r.1 .1 { e } else { r })
        .unwrap();
    info!("Guard {} slept {} times in minute {}", result.0, result.1.1, result.1.0);
    result.0 * result.1 .0 as usize // guard id * minute
}

#[derive(Debug, Default, Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
struct Date {
    month: u8,
    day: u8,
}

#[derive(Debug, Default, PartialEq, PartialOrd, Eq, Ord)]
enum EventT {
    Guard(usize),
    Asleep,
    Awake,
    #[default]
    None,
}

#[derive(Debug, Default, PartialEq, PartialOrd, Eq, Ord)]
struct Event {
    date: Date,
    minute: i8, // events in hour 23 of the previous day are recorded as negative minutes
    event: EventT,
}

type Events = Vec<Event>;

/// Sleepduration takes a list of events and delivers for each what the minute ranges were while
/// asleep in hour 0.
/// A sleep range is established when a Awake event is seen. If a Guard is known to be on duty and it is
/// known when he/she fell asleep, this duration is calculated and recorded with the Guard's id.
/// IF a Guard fell asleep and is releaved without waking up, the sleep time as assumed to have lasted up to
/// and including minute 59 of hour 0
fn find_sleep_ranges(events: &Events) -> HashMap<usize, Vec<Range<i8>>> {
    let mut result = HashMap::new();
    let mut guard_id = None;
    let mut asleep = None;
    for evt in events {
        match evt.event {
            EventT::None => continue,
            EventT::Guard(id) => {
                if guard_id.is_some() && asleep.is_some() {
                    // prev guard slept until 60
                    let asleep_ranges = result.entry(guard_id.unwrap()).or_insert(vec![]);
                    asleep_ranges.push(asleep.unwrap()..60);
                }
                guard_id = Some(id);
                asleep = None;
            }
            EventT::Asleep => asleep = Some(evt.minute),
            EventT::Awake => {
                if guard_id.is_some() && asleep.is_some() {
                    let asleep_ranges = result.entry(guard_id.unwrap()).or_insert(vec![]);
                    asleep_ranges.push(asleep.unwrap()..evt.minute);
                    asleep = None
                }
            }
        }
    }
    if log_enabled!(Level::Debug) {
        for guard in result.keys() {
            debug!("Guard {}: {:?}", guard, &result[guard]);
        }
    }
    result
}

/// Find the guard that slept the most according to his sleep ranges
fn find_sleepiest(ranges: &HashMap<usize, Vec<Range<i8>>>) -> usize {
    let mut max = std::i32::MIN;
    let mut sleepguard = None;
    for guard in ranges.keys() {
        let gslp = ranges[guard]
            .iter()
            .fold(0, |c, r| c + (r.end - r.start) as i32);
        if gslp > max {
            max = gslp;
            sleepguard = Some(guard);
            debug!("Found guard {} sleeping for {} minutes", guard, gslp);
        }
    }
    *sleepguard.unwrap()
}

// Find the most popular minute to be asleep according to a certain guard's sleep ranges
// Returns (the minute, the number of times the guard was asleep on that minute)
fn find_sleep_minute(ranges: &Vec<Range<i8>>) -> (i8, usize) {
    let mut mnt_cnt = vec![0; 60];
    for rng in ranges {
        for m in rng.start..rng.end {
            mnt_cnt[m as usize] += 1;
        }
    }
    let maxm = mnt_cnt.iter().max().unwrap();
    (mnt_cnt.iter().position(|m| m == maxm).unwrap() as i8, *maxm)
}

lazy_static! {
    static ref EVENTTM: Regex = Regex::new(r#"\[(\d+)-(\d+)-(\d+) (\d+):(\d+)\] "#).unwrap();
}

impl From<&str> for Event {
    /// Create an event from an event description.
    /// Panics if the description is not valid.
    fn from(input: &str) -> Self {
        let caps = EVENTTM.captures(input).unwrap();
        let rest = &input[caps[0].len()..];
        let month = caps[2].parse::<u8>().unwrap();
        let mut day = caps[3].parse::<u8>().unwrap();
        let hr = caps[4].parse::<u8>().unwrap();
        let mut mnt = caps[5].parse::<i8>().unwrap();
        let event = if rest == "wakes up" {
            EventT::Awake
        } else if rest == "falls asleep" {
            EventT::Asleep
        } else {
            let idstr = &rest.split_whitespace().nth(1).unwrap()[1..];
            let id = idstr.parse::<usize>().unwrap();
            EventT::Guard(id)
        };
        if hr == 23 {
            day += 1;
            mnt -= 60;
        }
        trace!(
            "Event with date: {}-{}, minute: {}, event: {:?}",
            month,
            day,
            mnt,
            event
        );
        Event {
            date: Date { month, day },
            minute: mnt,
            event,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    [1518-11-01 00:00] Guard #10 begins shift
    [1518-11-01 00:05] falls asleep
    [1518-11-01 00:25] wakes up
    [1518-11-01 00:30] falls asleep
    [1518-11-01 00:55] wakes up
    [1518-11-01 23:58] Guard #99 begins shift
    [1518-11-02 00:40] falls asleep
    [1518-11-02 00:50] wakes up
    [1518-11-03 00:05] Guard #10 begins shift
    [1518-11-03 00:24] falls asleep
    [1518-11-03 00:29] wakes up
    [1518-11-04 00:02] Guard #99 begins shift
    [1518-11-04 00:36] falls asleep
    [1518-11-04 00:46] wakes up
    [1518-11-05 00:03] Guard #99 begins shift
    [1518-11-05 00:45] falls asleep
    [1518-11-05 00:55] wakes up";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(240, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(4455, part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
