// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, warn, info, log_enabled, trace, Level, LevelFilter};

use std::collections::{ VecDeque, HashSet, HashMap };

const INPUTTXT: &str = include_str!("../input20.txt");

fn main() {
    let env = env_logger::Env::default();
    env_logger::Builder::from_env(env)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().map(|s|s.trim()).filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

/// Translate the string into a graph, with nodes and directed edges.
/// Each node is where an alternative exists or a path ends.
/// The length of the walk towards such spot denotes the value of the edge towards the node.
/// Each node will have an Id (its place in a Node vector), and a length (the number of NSEW from the previous node or the start)
/// We will be keeping a current path and its length, a construction stack and a build point.
/// So for each input char:
/// - ^ is the start
/// - N, S, E, W adds to the current edge's value, unless it is backtracing: then the previous length is kept
/// - ( identifies a node where 2 or more alternatives begin
///     The current path is recorded in the node vector
///     The Id is pushed on the construction stack
///     The Id is recorded as predessor for what is to come: the build point
///     The current node values are reset
/// - | identifies a new alternative starting at the last ( position )
///     The current path is recorded in the node vector
///     Construction stack remains
///     Build point is set to the construction stack's top.
///     The current node values are reset
/// - ) identifies the end of all alternatives of the last ( position.
///     If the current length is 0, this is an empty option, otherwise an alternative ended.
///     In both cases, the construction stack is popped. 
///     In case of an mepty option, the build point remains as is, otherwise, it set set to the construction stack's top.
/// - $ indicates the last end node. All alternatives should be closed at that point
/// 
/// Note that with an enpty option, the building continues producing a successor for the node completed at the last (
/// but at the next | or ) that node is forgotten and completed.
/// 
/// The first attempt, just assuming that no paths would touch each other, yielded too long paths.
/// Second attempt will know the coordinates and the available doors at each location
fn part1(input: &[&str]) -> usize {
    let mut old_len;
    // Check general format conditions of the input
    assert_eq!(1, input.len());
    debug!("Input length: {}", input[0].len());
    let mut ch_src = input[0].chars();
    assert_eq!('^', ch_src.next().unwrap());

    let mut map = Map::new();

    // nodes holds all nodes, ready or under construction.
    // All other places referencing nodes actually store indexes into this vec
    let mut nodes: Vec<Node>  = vec![];

    // constr holds the construction stack.
    let mut constr_id: Vec<Id> = vec![];
    let mut constr_pnt: Vec<Point> = vec![];
    let mut build_id = 0;
    old_len = 0;

    // The current nodeś data
    let mut curr_len = 0;
    let mut curr_path: Vec<char> = vec![];
    let mut curr_p = Point {x:0, y:0};
    map.locs.get_mut(&curr_p).unwrap().add_node(0);

    while let Some(ch) = ch_src.next() {
        let id = nodes.len();       // Id of the node being built.
        match ch {
            'N' | 'S' | 'E' | 'W' => {
                // If the path grows, curr.len is adjusted.
                // If not, the curr.len is maintained and the path shortens.
                // It is assumed that a diminishing path does not start growing again.
                // It is assumed that a diminishing path is part if a junction with an empty alternative
                curr_p = map.goto(&curr_p, Dir::from_ch(ch).unwrap());
                if growing_path(&mut curr_path, ch) {
                    curr_len += 1;
                    map.locs.get_mut(&curr_p).unwrap().add_node(id); // Do not try to add nodes to locations while backtracking
                }
                trace!("Map ({}) after '{}':\n{}", map.locs.len(), ch, map);
            }
            '(' => {
                // A new junction. The current node is complete and will have at least 2 successors
                nodes.push( Node { id, len: curr_len, succ: vec![], });
                if build_id != id {
                    // Declare the just completed node as a successor to someone, 
                    // unless the id is identical to the build point (such as with the first node)
                    nodes[build_id].succ.push(id);
                }
                // Push the completed node on to the construction stack to declare it as a parent for what is coming
                constr_id.push(id);
                constr_pnt.push(curr_p);
                build_id = id;
                // Start building a new node
                curr_len = 0;
                curr_path = vec![];       
            },
            '|' => {
                if curr_len > 0 {
                    // A path from the last junction ends here, and a new one starts
                    let parent = build_id;
                    build_id = *constr_id.last().unwrap();     // Make sure any empty option's effect are now gone
                    nodes.push( Node { id, len: curr_len, succ: vec![] } );
                    // Declare this node as a successor to its parent
                    nodes[parent].succ.push(id);
                }
                curr_len = 0;
                curr_path = vec![];
                curr_p = *constr_pnt.last().unwrap();
            },
            ')' => {
                if curr_len == 0 {
                    // found |): empty option
                    constr_id.pop().unwrap();      // Pop the start of the empty option range. Leave build point as is.
                } else {
                    // A node ends here, and its parent is complete
                    let parent = build_id;
                    nodes.push( Node { id, len: curr_len, succ: vec![] } );
                    // Declare this node as a successor to its parent
                    nodes[parent].succ.push(id);
                    constr_id.pop().unwrap();                  // End this construction frame
                    build_id = *constr_id.last().unwrap_or(&0);     // Build from the new construction top.
                    curr_len = 0;
                    curr_path = vec![];
                }
                curr_p = constr_pnt.pop().unwrap();
            },
            '$' => {
                // A path ends here, and the loop finishes
                if curr_len > 0 {
                    nodes.push( Node { id, len: curr_len, succ: vec![] } );
                    if build_id != id {
                        // Declare this node as a successor to its parent
                        nodes[build_id].succ.push(id);
                    }
                }
                break;
            },
            _ => panic!("This should never be reached"),
        }
        if old_len != nodes.len() {
            trace!("char: {}, build_pnt: {}, constr: {:?} @ ({},{})", ch, build_id, &constr_id, curr_p.x, curr_p.y);
            //trace!("Nodes:\n{}", nodes_as_string(&nodes));
            old_len = nodes.len();
        }
    }
    // debug!("Nodes:\n{}", nodes_as_string(&nodes));
    // trace!("{}", input[0] );
    debug!("Map: {} points:\n{}", map.locs.len(), map);
    let length1 = find_longest_path_in_nodes(&nodes);
    let (length2, num) = map.find_longest_path(1000);
    info!("Longest: {} (nodes) or {} (map)", length1, length2);
    info!("{num} rooms are at least 1000steps away");
    length2
}

fn part2(input: &[&str]) -> usize {
    0
}

type Id = usize;

#[derive(Debug, Clone)]
struct Node {
    id: Id,             // index into a list of nodes
    len: usize,         // length of edge towards the node
    succ: Vec<Id>       // All nodes reachable from this node
}

#[derive(Debug)]
struct Map {
    locs: HashMap<Point, Loc>,  // All locations in the room.
}

#[derive(Debug)]
struct Loc {
    p: Point,               // coordinates of the location
    dirs: HashSet<Dir>,     // Directions you can take from this locations
    nodes: Vec<Id>,         // Ids of all nodes that require this room to get to it, including the node itself.
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Dir {
    N = 0,
    E,
    S,
    W
}

/// Growing_path() takes a N, S, E, W characeter.
/// If it nullifies the last character in the path it pops the last from the path and returns false;
/// If not, the character is added, and true is returned as the path has grown
fn growing_path(path: &mut Vec<char>, new: char) -> bool {
    if path.len() == 0 {
        path.push(new);
        return true
    }
    let last = path.last().unwrap();
    match (new, last) {
        ('N','S') | ('S','N') | ('W','E') | ('E','W') => {
            path.pop().unwrap();
            false 
        },
        _ => {
            path.push(new);
            true
        }
    }
}

fn find_longest_path_in_nodes(nodes: &Vec<Node>) -> usize {
    struct State { 
        id: Id,     // Id of the node being observed 
        len: usize, // Steps needed to get to the predecessor node
        pred: Id,
    }

    let visited: Vec<Id> = Vec::with_capacity(nodes.len());
    let mut que: VecDeque<State> = VecDeque::new();
    let mut dists = vec![usize::MAX; nodes.len()];      // The shortest distance to the node
    que.push_back( State{ id: 0, len: 0, pred: 0, });

    while let Some(state) = que.pop_front() {
        let id = state.id;
        if visited.contains(&id) {
            continue;
        }
        let path_dist = state.len + nodes[id].len;
        if path_dist >= dists[id] {
            continue;
        } else {
            dists[id] = path_dist;
        }
        trace!("Looking at: {} at dist {} ({} + {}) with successors {:?} and pred {}", state.id, path_dist, state.len, nodes[id].len, nodes[id].succ, state.pred);
        for succ in &nodes[id].succ {
            que.push_back( State {
                        id: nodes[*succ].id,
                        len: path_dist,
                        pred: id
                    });
        } 
    }
    *dists.iter().max().unwrap()
}

fn nodes_as_string(nodes: &Vec<Node>) -> String {
    nodes.iter().map(|n| format!("{}", n)).collect::<Vec<_>>().join("\n")
}

impl Map {
    // Return a map with 1 Loc at poont (0,0)
    fn new() -> Map {
        let p0 = Point {x: 0, y: 0 };
        let mut map = Map {
            locs: HashMap::new(),
        };
        let loc0 = Loc {
            p: p0,
            dirs: HashSet::new(),
            nodes: vec![],
        };
        map.locs.insert(p0, loc0);
        map
    }

    /// From p go to the indicated direction.
    /// Add the taken direction to the src Loc, and the opposite to the target Loc
    /// Add the point to self.loc if not yet present.
    fn goto(&mut self, p: &Point, dir: Dir) -> Point{
        self.locs.get_mut(p).unwrap().dirs.insert(dir);       // Adjust p's directions. Mutable borrow ends at ;.
        let src = self.locs.get(p).unwrap();             // Get the Loc at p, immutable
        let oppos = dir.opposite();
        let dp = src.goto(dir);                        // A point which may or may not yet exist in the map
        let dest = self.locs.entry(dp).or_insert( Loc {
            p: dp,
            dirs: HashSet::new(),
            nodes: vec![],
        });
        dest.dirs.insert(oppos);
        dp
    }

    /// Find the shortest path to the most remote point in the map from the origin at (0,0)
    /// Any doors are available.
    /// Use Dijskstra's algorithm with a little twist.
    /// The vanilla flavor Dykstra uses a large unvisited collection to find the next node with the shortest distance.
    /// This leads to checking MANY nodes with distance infinity.
    /// It is MUCH more efficient to only scan all nodes that have been seen and therefore do not have a distance of infinity any more.
    /// Keeping track of seen nodes is easy:
    /// - The first one is the start node with distance 0. 
    /// - All successors of the node being inspected are put into the seen collection, after assigning a non-infinity distance to them
    /// - The visited node is removed from seen and removed from unvisited.
    /// Bonus: All disjoint nodes never make it into seen. If seen is empty all connected points have been visited.
    ///
    /// Return the shortest path too the most far away room AND the number of rooms reachable after num doors
    fn find_longest_path(&self, num: usize) -> (usize, usize) {    
        let mut dists: HashMap<Point, usize> = self.locs.iter().map(|(p, _)| (*p, usize::MAX)).collect();
        let mut pred: HashMap<Point, Option<Point>> = self.locs.iter().map(|(p, _)| (*p, None)).collect();
        let mut unvisited: HashSet<Point> = self.locs.iter().map(|(p, _)| *p ).collect();
        let mut seen: HashSet<Point> = HashSet::with_capacity(self.locs.len());

        let p0 = Point{ x:0, y:0 };
        dists.insert(p0, 0);
        pred.insert(p0, Some(p0));
        seen.insert(p0);
        
        while !seen.is_empty() {
            // Find the point with the smallest distance that is still only seen
            let min_d = seen.iter().map(|p| dists[p]).min().unwrap();
            let &min_pt = seen.iter().filter(|p| dists[p] == min_d).collect::<Vec<_>>()[0];
            // Update the distances and predecessors of all its neighbours
            let distance = dists[&min_pt];
            let location = &self.locs[&min_pt];
            let mut succ = vec![];      // For reporting
            for d in &location.dirs {
                let np = location.goto(*d);     // neighbour point
                if unvisited.contains(&np) {          // no back tracking
                    seen.insert(np);                  // neighbour is now seen
                    succ.push(np);                    // for reporting in trace!()
                    if dists[&np] > distance+1 {
                        dists.insert(np, distance + 1);
                        pred.insert(np, Some(min_pt));
                    }
                }
            }
            debug!("Found ({},{}) at dist {} with successors {:?} at dist {} and pred {:?}", min_pt.x, min_pt.y, min_d, succ, min_d+1, pred[&min_pt].unwrap());
            // Remove current point from unvisited and from seen
            unvisited.remove(&min_pt);
            seen.remove(&min_pt);

        }
        let furthest = dists.iter().map(|(_,d)| *d ).max().unwrap();
        let long     = dists.iter().filter(|(_,&d)| d >= num ).count();
        (furthest, long) 
    }
}

impl Loc {

    // From this Loc's point, find the point in the indicated direction
    fn goto(&self, dir: Dir) -> Point {
        let p = &self.p;
        match dir {
            Dir::N => Point {x: p.x, y: p.y-1 },
            Dir::S => Point {x: p.x, y: p.y+1 },
            Dir::E => Point {x: p.x+1, y: p.y },
            Dir::W => Point {x: p.x-1, y: p.y },
        }
    }

    /// Add a node ID to the Loc's node list.
    fn add_node(&mut self, id: Id) {
        if !self.nodes.contains(&id) {
            self.nodes.push(id);
        }
    }
}

impl Dir {

    /// Return the opposite direction
    fn opposite(&self) -> Self {
        use Dir::*; 
        match self {
            N => S,
            S => N,
            E => W,
            W => E,
        }
    }

    // Translate char to Dir
    fn from_ch(ch: char) -> Result<Dir, String> {
        match ch {
            'N' => Ok(Dir::N),
            'E' => Ok(Dir::E),
            'S' => Ok(Dir::S),
            'W' => Ok(Dir::W),
            _ => Err(format!("cannot build Dir from char '{ch}'"))
        }
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Id: {:5}, len: {:3}, succ: {:?}", self.id, self.len, self.succ)
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut vecp: Vec<_> = self.locs.iter().map(| (k, _)| *k).collect();
        vecp.sort();
        let sss = vecp.iter().map(|p| format!("{:?}", self.locs.get(p).unwrap())).collect::<Vec<_>>().join("\n");
        write!(f, "{sss}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

const INPUT01A: &str = "\
    ^WNE$
    3
    ";

const INPUT01B: &str = "\
    ^ENWWW(NEEE|SSE(EE|N))$
    10 
    ";

const INPUT01C: &str = "\
    ^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$ 
    18
    ";

const INPUT01D: &str = "\
    ^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$ 
    23
    ";

const INPUT01E: &str = "\
    ^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$
    31 
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        let tests = [INPUT01A, INPUT01B, INPUT01C, INPUT01D, INPUT01E];
        init_logger();
        let mut passed = true;
        for (n, test) in tests.iter().enumerate() {
            let tst: Vec<_> = test.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
            let input = tst[0];
            let want: usize = tst[1].parse().unwrap();
            info!("Test nr 1.{}", n);
            info!("String: {}", input);
            let got = part1(&[input]);
            if want ==  got {
                continue;
            } else {
                println!("Test 1.{n}: wanted {want}, got {got}");
            }
            passed = false;
        }
        assert!(passed);
    }

    // #[test]
    // fn test2() {
    //     init_logger();
    //     let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
    //     assert_eq!(1, part2(&input));
    // }

    fn init_logger() {
        let env = env_logger::Env::default();
        env_logger::Builder::from_env(env)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .init();
    }
}
