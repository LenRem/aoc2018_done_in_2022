// Needed use statements for scaffolding
use std::fmt::Debug;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

const INPUT: usize = 5535;

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    run_and_report("Part1", &part1, INPUT);
    run_and_report("Part2", &part2, INPUT);
}

fn run_and_report<R: Debug>(name: &str, f: &dyn Fn(usize) -> R, input: usize) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {:?} in {elapsed} ms", result);
}

fn part1(sn: usize) -> ((usize, usize), i64) {
    const SIZE: usize = 300;
    let mut maxplvl = i64::MIN;
    let mut xy = (0, 0);
    for x0 in 1..=SIZE - 2 {
        for y0 in 1..=SIZE - 2 {
            let mut sum = 0_i64;
            for x in x0..(x0 + 3) {
                for y in y0..(y0 + 3) {
                    let plvl = power_level(sn, x, y);
                    sum += plvl;
                }
            }
            if sum > maxplvl {
                maxplvl = sum;
                xy = (x0, y0);
            }
        }
    }
    (xy, maxplvl)
}

// Just throwing in a formsz in 1..=SIZE loop is a very time consuminng busines:
// The number of power level lookups is (SIZE-sz+1)(SIZE-sz+1)*sz*sz: with sz = SIZE/2 this is around 60_000_000 lookups
// (237,284,11) is the right answer, after 1 hr 19 minutes of doing the brute force thing
// One can also drag a square over the grid, substract the trailng side values and add the leading side values.
// If you store the values for the upper row drag, yuo can also drag down
fn part2(sn: usize) -> ((usize, usize, usize), i64) {
    const SIZE: usize = 300;
    let mut maxplvl = i64::MIN;
    let mut xysz = (0, 0, 0);
    for sz in 1..=SIZE {
        // print!("   {:5}\r", sz);
        // std::io::stdout().flush().unwrap();

        let mut row: Vec<i64> = vec![0; SIZE - sz + 2]; // sz == 1 : SIZE+1, sz == SIZE : 2: allow for not using 0

        // calculate the power level of the sz * sz grid at (1,1)
        let mut sum = 0;
        for x in 1..=sz {
            for y in 1..=sz {
                let plvl = power_level(sn, x, y);
                sum += plvl;
            }
        }
        if sum > maxplvl {
            maxplvl = sum;
            xysz = (1, 1, sz);
            debug!("New best: @ {:?}: {}", xysz, sum);
        }
        row[1] = sum;

        // drag the square to the right, on row 1.
        // sz == 1: SIZE times, sz == SIZE: 1: done, sz == SIZE-1: 2: one time
        for x0 in 2..=(SIZE - sz + 1) {
            // calculate the trailing side's sum and the leading side's sum
            let trail: i64 = (1..=sz).map(|y| power_level(sn, x0 - 1, y)).sum();
            let lead: i64 = (1..=sz).map(|y| power_level(sn, x0 - 1 + sz, y)).sum();
            sum = sum + lead - trail;
            if sum > maxplvl {
                maxplvl = sum;
                xysz = (x0, 1, sz);
                debug!("New best: @ {:?}: {}", xysz, sum);
            }
            row[x0] = sum;
        }
        // debug!("Last used: {}: {:?}", SIZE - sz + 1, row);
        // drag the top row down
        for y0 in 2..=(1 + SIZE - sz) {
            for x0 in 1..=(1 + SIZE - sz) {
                sum = row[x0];
                let trail: i64 = (x0..(x0 + sz)).map(|x| power_level(sn, x, y0 - 1)).sum();
                let lead: i64 = (x0..(x0 + sz))
                    .map(|x| power_level(sn, x, y0 - 1 + sz))
                    .sum();
                sum = sum + lead - trail;
                if sum > maxplvl {
                    maxplvl = sum;
                    xysz = (x0, y0, sz);
                    debug!("New best: @ {:?}: {}", xysz, sum);
                }
                row[x0] = sum; // ready for next row
            }
        }
    }
    (xysz, maxplvl)
}

// Calculates the power level of the cell at (x, y) on a grid with serial number sn
// Tried caching this, as each point is calculated numerous times.
// It turns out that a HashMap lookup is more expensive than just calculating the powerlevel.
fn power_level(sn: usize, x: usize, y: usize) -> i64 {
    let id = x + 10;
    let plvl = id * (id * y + sn);
    let plvl: i64 = ((plvl / 100) % 10) as i64 - 5;
    plvl
}

#[cfg(test)]
mod tests {
    use super::*;

    // sn, x, y, --> power level
    const INPUT1A: &[i64] = &[
        8, 3, 5, 4, 57, 122, 79, -5, 39, 217, 196, 0, 71, 101, 153, 4,
    ];

    // sn, --> x, y, power level
    const INPUT1B: &[i64] = &[18, 33, 45, 29, 42, 21, 61, 30];

    #[test]
    /// Test individual shuffling techniques
    fn test1a() {
        init_logger();
        for nt in 0..(INPUT1A.len() / 4) {
            let sn = INPUT1A[nt * 4 + 0] as usize;
            let x = INPUT1A[nt * 4 + 1] as usize;
            let y = INPUT1A[nt * 4 + 2] as usize;
            let expect = INPUT1A[nt * 4 + 3];
            debug!("Test 1a-{}: SN: {}, ({},{}) --> {} ?", nt, sn, x, y, expect);
            assert_eq!(expect, power_level(sn, x, y));
        }
    }

    #[test]
    fn test1b() {
        init_logger();
        for nt in 0..(INPUT1B.len() / 4) {
            let sn = INPUT1B[nt * 4 + 0] as usize;
            let exp_x = INPUT1B[nt * 4 + 1] as usize;
            let exp_y = INPUT1B[nt * 4 + 2] as usize;
            let exp_pwr = INPUT1B[nt * 4 + 3];
            debug!(
                "Test 1a-{}: SN: {} --> ({},{}): {} ?",
                nt, sn, exp_x, exp_y, exp_pwr
            );
            assert_eq!(((exp_x, exp_y), exp_pwr), part1(sn));
        }
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
