// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::{HashMap, VecDeque};    // {HashSet, HashMap, VecDeque};

use lazy_static::lazy_static;
use regex::Regex;   

const INPUTTXT: &str = include_str!("../input17.txt");

// const LOGLEVEL: LevelFilter = LevelFilter::Info;
const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().map(|s|s.trim()).filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let mut map = Map::new(input);
    debug!("Dry map: ({},{}) --> ({},{}), miny: {}, maxy: {}\n{}", map.tl.x, map.tl.y, map.br.x, map.br.y, map.miny, map.maxy, map.to_string(None));
    map.make_wet();
    debug!("Wet map:\n{}", map.to_string(None));
    map.count_wet()
}

fn part2(input: &[&str]) -> usize {
    let mut map = Map::new(input);
    map.make_wet();
    map.count_trapped()
}

#[derive(Debug)]
struct Map {
    locs: Vec<Vec<LocT>>,
    tl: Point,      // first row and column to store and print: locs[0][0] is at tl.
    br: Point,      // last row and cpolumn to store and print: locs[last][last] is at br.
    miny: usize,    // 1st row to count water on
    maxy: usize,    // last row to count water on
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
enum LocT {
    Clay,
    #[default]
    Sand,
    Wtr(Water),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Water {
    Trapped,
    Flowing,
}

const SPRING: Point = Point { x: 500, y: 0 };

impl Map {
    /// Read a vector of well formatted strings, and construct a dry Map out of it
    fn new(inp: &[&str]) -> Self {
        lazy_static! {
            static ref RE: Regex = Regex::new(r#"([xy])=(\d+), ([xy])=(\d+)\.\.(\d+)"#).unwrap();
            //                            group  1:xy  2:nr0    3:xy  4:nr1.0  5:nr 1.1
        }
        let mut points: Vec<Point> = Vec::with_capacity(inp.len());          
        for (n, line) in inp.into_iter().enumerate() {
            let caps = RE.captures(line).unwrap();
            let ax0 = caps.get(1).map(|m| m.as_str()).unwrap();
            let nr0 = caps.get(2).map(|m| m.as_str().parse::<usize>().unwrap()).unwrap();
            let ax1 = caps.get(3).map(|m| m.as_str()).unwrap();
            let nr1_start = caps.get(4).map(|m| m.as_str().parse::<usize>().unwrap()).unwrap();
            let nr1_end = caps.get(5).map(|m| m.as_str().parse::<usize>().unwrap()).unwrap();
            if ax0 == ax1 {
                panic!("{} used twice on line {}", ax0, n+1)
            }
            let pnts: Vec<_>  = if ax0 == "x" {
                let x = nr0;
                (nr1_start..=nr1_end).map(|y| Point { x, y }).collect()
            } else {
                let y = nr0;
                (nr1_start..=nr1_end).map(|x| Point{ x, y }).collect()
            };
            points.extend(pnts);
        }
        let minx = points.iter().map(|p| p.x ).min().unwrap();
        let maxx = points.iter().map(|p| p.x ).max().unwrap();
        let miny = points.iter().map(|p| p.y ).min().unwrap();
        let maxy = points.iter().map(|p| p.y ).max().unwrap();
        let tlx = if minx > 0 { minx - 1 } else { minx };
        let tl = Point{ x: tlx , y: 0 };
        let br = Point{ x: maxx + 1, y: maxy + 1 };
        let szx = br.x - tl.x + 1;
        let szy = br.y -tl.y + 1;
        let mut locs: Vec<Vec<LocT>> = vec![vec![LocT::default();szx];szy];
        for p in points {
            locs[p.y-tl.y][p.x-tl.x] = LocT::Clay;
        }
        Self { 
            locs,
            miny,
            maxy,
            tl,
            br,
        }
    }

    // return a reference to the LocT at world coordninates
    fn get_locs<'a>(&'a self, p: &Point) -> &'a LocT {
        &self.locs[p.y-self.tl.y][p.x-self.tl.x]
    }

    // return a mutable reference to the LocT at world coordninates
    fn getmut_locs<'a>(&'a mut self, p: &Point) -> &'a mut LocT {
        &mut self.locs[p.y-self.tl.y][p.x-self.tl.x]
    }

    /// Transform the map (dry or wet) into a string that can be printed
    fn to_string(&self, from_to: Option<(usize, usize)>) -> String {
        let (y0, y1) = match from_to {
            Some((a, b)) if b > a => (a - a.min(10), self.locs.len().min(b+10)),
            _ => (0, self.locs.len()),
        };
        let mut vecstr: Vec<String> = Vec::with_capacity(y1 - y0);
        for y in y0..y1 {
            vecstr.push(format!("{y:4}: {}", (0..self.locs[y].len()).map(| x |
                 self.locs[y][x].as_char(y+self.tl.y == SPRING.y  && x+self.tl.x == SPRING.x)).collect::<String>())); 
        }
       vecstr.join("\n")
    }

    /// Let water from the spring flow into the map, transforming a dry map into a wet map
    /// Replace all LocT that get wet with the appropriate LocT (´|' or ´~´)
    /// Start at the spring. It is our first drop point. Push it.
    /// Pop a drop point.
    ///     Find the bottom (clay or ymax)
    ///         If ymax: done
    ///         if clay:
    ///             repeat:
    ///                 find left and right edges
    ///                 if both clay: 
    ///                     make trapped water.
    ///                     Go one up
    ///                 if 1 or 2 are drop points: 
    ///                     push droppoints
    ///                     done. 
    fn make_wet(&mut self) {
        struct QueSts {
            drop_pnt: Point, 
        } 
        let mut lowesty = 0;
        let mut que : VecDeque<QueSts> = VecDeque::new();
        que.push_back( QueSts { drop_pnt: SPRING });
        let mut visited: Vec<Point> = vec![];
        while let Some(sts) = que.pop_front() {
            debug!("Droppoint: ({},{})", sts.drop_pnt.x, sts.drop_pnt.y);
            let droppnt = sts.drop_pnt;
            visited.push(droppnt);
            if let Some(bottom) = (droppnt.y..=self.maxy)
                                            .map(|y| Point { x: droppnt.x, y })
                                            .position(|p| !self.get_locs(&p).is_flowable() ) {
                // found clay or trapped water
                let mut bottom = droppnt.y + bottom;
                debug!("Splash at ({},{})", droppnt.x, bottom);
                lowesty = bottom;
                // Make wet from drop point until bottom
                (droppnt.y..bottom).for_each(|y| *self.getmut_locs(&Point{ x: droppnt.x, y} ) = LocT::Wtr(Water::Flowing));
                // find edges or next droppoint(s)
                loop {
                    bottom -= 1;    // Move up 1 row
                    let (ledge, redge ) = self.find_hor_edges(droppnt.x, bottom);
                    debug!("Edges @ {}: {} and {}", bottom, ledge, redge);
                    if self.locs[bottom-self.tl.y][ledge - self.tl.x] == LocT::Clay && self.locs[bottom-self.tl.y][redge - self.tl.x] == LocT::Clay {
                        (ledge+1..redge).for_each(|x| *self.getmut_locs(&Point{ x, y:bottom } ) = LocT::Wtr(Water::Trapped));
                        continue;
                    }
                    // One or two drop sides
                    (ledge+1..redge).for_each(|x| *self.getmut_locs(&Point{ x, y:bottom } ) = LocT::Wtr(Water::Flowing));
                    if self.locs[bottom-self.tl.y][ledge - self.tl.x] != LocT::Clay {
                        let pushpoint = Point {x: ledge, y: bottom};
                        if !visited.contains(&pushpoint){
                            visited.push(pushpoint);
                            que.push_back( QueSts { drop_pnt: pushpoint});
                            debug!("Push ({ledge},{bottom})");
                        }
                    }
                    if self.locs[bottom-self.tl.y][redge - self.tl.x] != LocT::Clay {
                        let pushpoint = Point {x: redge, y: bottom};
                        if !visited.contains(&pushpoint){
                            visited.push(pushpoint);
                            que.push_back( QueSts { drop_pnt: pushpoint});
                            debug!("Push ({redge},{bottom})")
                        }
                    }
                    break;
                }
            } else {
                // dropped off the map. Make the trail wet and be done.
                (droppnt.y..=self.maxy).for_each(|y| self.locs[y-self.tl.y][droppnt.x-self.tl.x] = LocT::Wtr(Water::Flowing));
            }
            debug!("\n{}", self.to_string(Some((droppnt.y, lowesty))));
        }
    }

    fn find_hor_edges(&self, x0: usize, y0: usize ) -> (usize, usize) {
        let mapy0 = y0 - self.tl.y;
        let mapx0 = x0 - self.tl.x;
        let maxy = self.locs.len()-1;
        let ledge = directed_range(mapx0, 0)
                .position(|x| self.locs[mapy0][x] == LocT::Clay ||
                                    self.locs[maxy.min(mapy0+1)][x].is_flowable()).unwrap();
        let redge = directed_range(mapx0, self.br.x - self.tl.x + 1)
                .position(|x| self.locs[mapy0][x] == LocT::Clay ||
                                    self.locs[maxy.min(mapy0+1)][x].is_flowable() ).unwrap();

        (x0 - ledge, x0 + redge)
    }   

    /// Count wet cells between miny and maxy (inclusive)
    fn count_wet(&self) -> usize {
        let mut count = 0;
        for row in &self.locs[self.miny..=self.maxy] {
            count += row.iter().filter(|loc| if let LocT::Wtr(_) = loc {true} else {false} ).count();
        }
        count
    }

    /// Count trapped wter cells between miny and maxy (inclusive)
    fn count_trapped(&self) -> usize {
        let mut count = 0;
        for row in &self.locs[self.miny..=self.maxy] {
            count += row.iter().filter(|loc| if let LocT::Wtr(Water::Trapped) = loc {true} else {false} ).count();
        }
        count
    }
}

impl LocT {
    fn as_char(&self, is_src: bool) -> char {
        if is_src { '+' }
        else { match self {
            LocT::Sand => '.',
            LocT::Clay => '#', 
            LocT::Wtr(wtr) => match wtr {
                Water::Trapped => '~',
                Water::Flowing => '|',
            }
        }}
    }

    fn is_flowable(&self) -> bool {
        match self {
            LocT::Sand => true,
            LocT::Clay => false,
            LocT::Wtr(Water::Trapped) => false,
            LocT::Wtr(Water::Flowing) => true,
        }
    }
}

/// Return an iterator that can goes up OR DOWN from a to b (inclusive)
fn directed_range(a: usize, b: usize) -> impl Iterator<Item = usize> {
    let mut start = a;
    let end = b;
    let mut ending = false;
    std::iter::from_fn(move || {
            use std::cmp::Ordering::*;
            if ending { None } else { 
                match start.cmp(&end) {
                    Less => {
                        start += 1;
                        Some(start - 1)
                    }
                    Equal => {
                        ending = true;
                        Some(end)
                    },
                    Greater => {
                        start -= 1;
                        Some(start + 1)
                    }
                }
            }
        }
    )
}
#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    x=495, y=2..7
    y=7, x=495..501
    x=501, y=3..7
    x=498, y=2..4
    x=506, y=1..2
    x=498, y=10..13
    x=504, y=10..13
    y=13, x=498..504
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(57, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(29, part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
