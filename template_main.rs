// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../inputXX.txt");

fn main() {
    let env = env_logger::Env::default();
    env_logger::Builder::from_env(env)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().map(|s|s.trim()).filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    0
}

fn part2(input: &[&str]) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
        ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(1, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(1, part2(&input));
    }

    fn init_logger() {
        let env = env_logger::Env::default();
        env_logger::Builder::from_env(env)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init().ok();
    }
}
