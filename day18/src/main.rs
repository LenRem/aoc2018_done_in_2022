// Needed use statements for scaffolding
use std::{fmt::Display, io::LineWriter};
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections:: HashMap;   //{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input18.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().map(|s|s.trim()).filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let mut yard = Yard::new(input);
    debug!("Yard: ({}x{}):\n{}", yard.map[0].len(), yard.map.len(), yard);
    for t in 1..=10 {
        yard.map = yard.age();
        let score = yard.score();
        debug!("After aging period {t}:\n{score}")
    }
    debug!("FInal map:\n{yard}");
    yard.num_loc_trees() * yard.num_loc_lumber()

}

/// There will be a repetitive set of yard scores.
/// First find a repeated score.
/// Then in an inner loop: 
///     Find scores and confirm they are all already there.
///     If any new score is found: break into the outer loop
///     If the starting repeat is found again:
///         Record start of repeat: age0
///         Record end of repeat: age1
///         
fn part2(input: &[&str]) -> usize {
    let target: usize = 1_000_000_000;
    let mut yard = Yard::new(input);
    let mut scores2age: HashMap<usize,usize> = HashMap::new();
    let mut age = 0;
    loop {
        let (age0, age1, score0) = yard.find_start_of_repetitions(&mut scores2age, age);
        scores2age.insert(score0, age1);     // Update score to latter occurrence
        let period = age1 - age0;
        let mut scores: Vec<usize> = Vec::with_capacity(period+1);        // Scores from age1 or age0 onward
        scores.push(score0);
        age = age1;
        loop {
            // Keep aging and updating but check for any new score. Also maintain an age to score map from age1 onward.
            age += 1;
            yard.map = yard.age();
            let score = yard.score();
            debug!("Checking repeat score at age {age}: {score}");
            scores.push(score);
            if scores2age.insert(score, age).is_none() {
                // A new score was found
                break;
            }
            if score == score0 {
                // We found the repesting sequence. Where im this sequene would the target age be?
                let idx = (target - age0) % period;
                info!("Map at age {age}:\n{}", yard);
                return scores[idx];
            }
        }
    }
}

struct Yard {
    map: YardMap,
    szx: usize,
    szy: usize,
}
 type YardMap = Vec<Vec<LocT>>;

#[derive(Debug,PartialEq)]
enum LocT {
    Open,
    Trees,
    Lumber,
}

impl Yard {
    /// Create a map from a slice of &str
    fn new(input: &[&str]) -> Self {
        let szy = input.len();
        let mut map :  YardMap = Vec::with_capacity(szy);
        for line in input.iter() {
            map.push( line.chars().map(|c| LocT::from(c)).collect::<Vec<_>>());
        }
        let szx = map[0].len();
        Yard { map, szx, szy }
    }

    /// Create an empty map (all LocT::Open) with the same size as self.
    fn empty_map(&self) ->  YardMap {
        let mut map :  YardMap = Vec::with_capacity(self.szy);
        for _ in 0..self.szy {
            map.push( (0..self.szx).map(|_| LocT::Open).collect::<Vec<_>>());
        }
        map
    }

    /// Age all items of the Yard 
    fn age(& self) -> YardMap {
        let mut new_map = self.empty_map();
        for y in 0..self.szy {
            for x in 0..self.szx {
                new_map[y][x] = self.aged_item(x, y)
            }
        }
        new_map
    }

    fn aged_item(&self, x0: usize, y0: usize) -> LocT {
        let mut num_trees = 0;
        let mut num_lumber = 0;
        let xmin = if x0 > 0 { x0 - 1 } else { x0 };
        let xmax = if x0 < self.szx-1 { x0 + 2 } else { x0 + 1 };
        let ymin = if y0 > 0 { y0 - 1 } else { y0 };
        let ymax = if y0 < self.szy-1 { y0 + 2 } else { y0 + 1 };
        for y in ymin..ymax {
            for x in xmin..xmax {
                if (x,y) == (x0,y0) { 
                } else {
                    match self.map[y][x] {
                        LocT::Open => (),
                        LocT::Trees => num_trees += 1,
                        LocT::Lumber => num_lumber += 1,
                    }
                }
            }
        }
        match self.map[y0][x0] {
            LocT::Open => if num_trees >= 3 { LocT::Trees } else { LocT::Open },
            LocT::Trees => if num_lumber >= 3 { LocT::Lumber } else { LocT::Trees },
            LocT::Lumber => if num_trees >= 1 && num_lumber >= 1 { LocT::Lumber } else { LocT::Open },
        }
    }

    fn num_loc_trees(&self) -> usize {
        self.map.iter().map(|row| row.iter().filter(|loc| **loc == LocT::Trees ).count()).sum()
    }

    fn num_loc_lumber(&self) -> usize {
        self.map.iter().map(|row| row.iter().filter(|loc| **loc == LocT::Lumber ).count()).sum()
    }

    fn score(&self) -> usize {
        self.num_loc_trees() * self.num_loc_lumber()
    }

    /// Age the yard, until a score is repeated.
    /// The input yard and scores map  is assumed to be based on start completed itertions
    /// The yard and the score card are adapted as aging progresses
    /// The age of the first occurrence of the score, and the age of reoccurrence,  and the score itself are returned.
    /// The scores map is not updated for the second occurrence of a score
    fn find_start_of_repetitions(&mut self, scores: &mut HashMap<usize, usize>, start: usize) -> (usize, usize, usize) {
        let mut age = start;
        loop {
            age += 1;
            self.map = self.age();
            let score = self.score();
            debug!("After aging period {age}: {score}");
            if scores.contains_key(&score) {
                break (*scores.get(&score).unwrap(), age, score);
            }
            scores.insert(score, age);
        } 
    }

}

impl LocT {
    fn as_char(&self) -> char {
        match self {
            LocT::Open => '.',
            LocT::Trees=> '|',
            LocT::Lumber=> '#',
        }
    }
}

impl Display for Yard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut strvec = Vec::with_capacity(self.szy);
        for y in 0..self.szy {
            strvec.push(self.map[y].iter().map(|loc| loc.as_char()).collect::<String>());
        }
        write!(f, "{}", strvec.join("\n"))
    }
}

impl From<char> for LocT {
    fn from(c: char) -> Self {
        match c {
            '#' => LocT::Lumber,
            '|' => LocT::Trees,
            _   => LocT::Open,
        }
    }
} 

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    .#.#...|#.
    .....#|##|
    .|..|...#.
    ..|#.....#
    #.#|||#|#|
    ...#.||...
    .|....|...
    ||...#|.#|
    |.||||..|.
    ...#.|..|.
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(1147, part1(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
