// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input19.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> i32 {
    let mut cpu = Cpu::new(input);
    let mut count = 0;
    while let Ok(()) = cpu.exec() { count += 1; }
    info!("Register 0: {} after {count} instructions", cpu.regs[0]);
    debug!("\n{}", &cpu.trace2str(&(0..50)));
    
    cpu.regs[0]
}

/// Well, part 1 is nice, but part 2 just takes way too long.
/// Manually decoding the program, it translates to something like this:
/// ```no code
/// let R4 = if R1 == 0 { 906 } else if R1 == 1 { 10551306 } else { undefined }
/// let R0 = 0;
/// for R5 in (1..=R4) {
///     for R2 in (1..=R4) {
///         if R2 * R5 == R4 {  
///             R0 += R5; // Add all dividers of R4
///         }
///     }
/// }
/// ```
/// The program effectively adds all dividers of the key value.
/// 906 has prime factors 2,3,151 so dividers 1,2,3,6,151,302,453 and 906, that add up to 1824.
/// 10551306 has primes 2*3*89*19759. That makes dividends 
/// 1,2,3,6,89,178,267,534,19759,39518,59277,118554,1758551,3517102,5275653,10551306 that add up to 21340800
/// This part 2 let's the program run until R4 has the key value, then finds all dividers and sums them.
 fn part2(input: &[&str]) -> i32 {
    let mut cpu = Cpu::new(input);
    cpu.regs[0] = 1;
    for _ in 0..20 {
        cpu.exec().unwrap(); 
    }
    let key = cpu.regs[4];
    info!("Key value is {key}");
    (1..((key as f64).sqrt() as i32)).filter(|d| key % d == 0 ).map(|d| d + key/d ).sum()
}

const NUMREGS: usize = 6;

type Regs = [i32; NUMREGS];


#[derive(Debug, Clone, Copy)]
struct Instr {
    oper: Operation,
    a: i32,
    b: i32,
    trgt: usize
}

#[derive(Debug)]
struct TraceItem {
    nr: usize,
    instr: Instr,
    regs: Regs,
}

type Program = Vec<Instr>;


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Operation {
    ADDR, // rC := rA + rB
    ADDI, // rC := rA + vB
    MULR, // rC := rA * rB
    MULI, // rC := rA * vB
    BANR, // rC := rA & rB
    BANI, // rC := rA & vB
    BORR, // rC := rA | rB
    BORI, // rC := rA | vB
    SETR, // rC := rA     // ignore rB
    SETI, // rC := vA     // ignroe rB
    GTIR, // rC := vA > rB
    GTRI, // rC := rA > vB
    GTRR, // rC := rA > rB
    EQIR, // rC := vA == rB
    EQRI, // rC := rA == vB
    EQRR, // rC := rA == rB
}

#[derive(Debug)]
struct Cpu {
    regs: Regs,             // current register set
    ip: usize,              // value of instruction pointer
    p_ip: usize,            // index of register that acts as instruction pointer
    program: Program,
    trace: Vec<TraceItem>,
}

impl Instr {
    fn new(line: &str) -> Self {
        let mut line = line.split_whitespace();
        let oper = Operation::new(line.next().unwrap()).unwrap();
        let a: i32 = line.next().unwrap().parse().unwrap();
        let b: i32 = line.next().unwrap().parse().unwrap();
        let trgt: usize = line.next().unwrap().parse().unwrap();
        Instr{ oper, a, b, trgt }
    }
}

impl Operation {
    fn exec(&self, cmd: &Instr, regs: &Regs) -> Regs {
        let mut outregs = *regs;
        let output = &mut outregs[cmd.trgt];
        use Operation::*;
        match self {
            ADDR => *output = regs[cmd.a as usize] + regs[cmd.b as usize],
            ADDI => *output = regs[cmd.a  as usize] + cmd.b,
            MULR => *output = regs[cmd.a  as usize] * regs[cmd.b as usize],
            MULI => *output = regs[cmd.a  as usize] * cmd.b,
            BANR => *output = regs[cmd.a  as usize] & regs[cmd.b as usize],
            BANI => *output = regs[cmd.a  as usize] & cmd.b,
            BORR => *output = regs[cmd.a  as usize] | regs[cmd.b as usize],
            BORI => *output = regs[cmd.a  as usize] | cmd.b,
            SETR => *output = regs[cmd.a  as usize],
            SETI => *output = cmd.a ,
            GTIR => *output = if cmd.a  > regs[cmd.b as usize] { 1 } else { 0 },
            GTRI => *output = if regs[cmd.a  as usize] > cmd.b { 1 } else { 0 },
            GTRR => {
                *output = if regs[cmd.a as usize] > regs[cmd.b as usize] {
                    1
                } else {
                    0
                }
            }
            EQIR => {
                *output = if cmd.a == regs[cmd.b as usize] {
                    1
                } else {
                    0
                }
            }
            EQRI => {
                *output = if regs[cmd.a as usize] == cmd.b {
                    1
                } else {
                    0
                }
            }
            EQRR => {
                *output = if regs[cmd.a as usize] == regs[cmd.b as usize] {
                    1
                } else {
                    0
                }
            }
        }
        // debug!("Executing");
        outregs
    }

    // fn iter() -> impl Iterator<Item = &'static Operation> {
    //     use Operation::*;
    //     [
    //         ADDR, ADDI, MULR, MULI, BANR, BANI, BORR, BORI, SETR, SETI, GTIR, GTRI, GTRR, EQIR,
    //         EQRI, EQRR,
    //     ]
    //     .iter()
    // }

    fn new(inp: &str) -> Result<Operation, String> {
        use Operation::*;
        match inp {
            "addr"  =>    Ok(ADDR),
            "addi"  =>    Ok(ADDI),
            "mulr"  =>    Ok(MULR),
            "muli"  =>    Ok(MULI),
            "banr"  =>    Ok(BANR),
            "bani"  =>    Ok(BANI),
            "borr"  =>    Ok(BORR),
            "bori"  =>    Ok(BORI),
            "setr"  =>    Ok(SETR),
            "seti"  =>    Ok(SETI),
            "gtir"  =>    Ok(GTIR),
            "gtri"  =>    Ok(GTRI),
            "gtrr"  =>    Ok(GTRR),
            "eqir"  =>    Ok(EQIR),
            "eqri"  =>    Ok(EQRI),
            "eqrr"  =>    Ok(EQRR),

            _  => Err(format!("Invalid string '{}'", inp))
        }
    }
}

impl Cpu {

    /// Create a CPU from a vector of text lines.
    /// The first line contains "#ip <n>" and fingers the register that contains the instruction pointer
    /// The other lines contain instructions: <opcode> <A> <B> <C>
    fn new(input: &[&str]) -> Self {
        let mut input = input.into_iter();
        let &line1 = input.next().unwrap();
        let p_ip = line1.split_whitespace().nth(1).unwrap().parse::<usize>().unwrap();
        let program: Vec<_> = input.map(|&line| Instr::new(line)).collect();
        Self { 
            regs: [0;NUMREGS],
            ip: 0,
            p_ip: p_ip,
            program,
            trace: vec![],
        }
    }

    fn exec(&mut self) -> Result<(), String> {
        self.regs[self.p_ip] = self.ip as i32;
        let instr = &self.program[self.ip];
        self.regs = instr.oper.exec( &instr, &self.regs);
        self.trace.push(TraceItem { nr: self.trace.len() + 1, instr: instr.clone(), regs: self.regs });
        let maybe_ip = self.regs[self.p_ip] + 1;
        if maybe_ip < 0 ||  maybe_ip as usize >= self.program.len() {
            return Err(format!("Program counter outside program"));
        }
        self.ip = maybe_ip as usize;
        Ok(())
    }

    fn trace2str(&self, range: &std::ops::Range::<usize>) -> String {
        let rng = std::ops::Range {start: range.start, end: range.end.min(self.trace.len()) };
        let ss: Vec<_> = rng.map(|i| self.trace[i].as_string()).collect();
        ss.join("\n")
    } 
}

impl TraceItem {
    fn as_string(&self) -> String {
        format!("{:7}: {} {} {} {} -> {:?}", self.nr, self.instr.oper, self.instr.a, self.instr.b, self.instr.trgt, self.regs)    
    }
}

impl Display for Operation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Operation::*;
        let s = match &self {
            ADDR => "addr",
            ADDI => "addi",
            MULR => "mulr",
            MULI => "muli",
            BANR => "banr",
            BANI => "bani",
            BORR => "borr",
            BORI => "bori",
            SETR => "setr",
            SETI => "seti",
            GTIR => "gtir",
            GTRI => "gtri",
            GTRR => "gtrr",
            EQIR => "eqir",
            EQRI => "eqri",
            EQRR => "eqrr",
        };
        write!(f, "{s}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
 
    const INPUT01: &str = "\
    #ip 0
    seti 5 0 1
    seti 6 0 2
    addi 0 1 0
    addr 1 2 3
    setr 1 0 0
    seti 8 0 4
    seti 9 0 5
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(6, part1(&input));
    }    

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
