// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::HashSet; // {HashSet, HashMap, VecDeque};

use lazy_static::lazy_static;
use regex::Regex;

const INPUTTXT: &str = include_str!("../input03.txt");

// const LOGLEVEL: LevelFilter = LevelFilter::Info;
const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let claims: Claims = make_claims(input);
    count_double_points(&claims)
}

fn part2(input: &[&str]) -> usize {
    let claims: Claims = make_claims(input);
    if let Some(id) = find_unique_claims(&claims) {
        id
    } else {
        0
    }
}

#[derive(Debug, Default, Clone, Copy, Eq, PartialEq, Hash)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Debug, Default, Clone, Copy)]
struct Claim {
    id: usize,
    tl: Point,
    br: Point,
}

#[derive(Clone, Copy)]
struct Area {
    tl: Point,
    br: Point,
}

type Claims = Vec<Claim>;
type PointSet = HashSet<Point>;

/// Create claims out of a series of claim definitions
fn make_claims(input: &[&str]) -> Claims {
    let claims: Claims = input
        .iter()
        .map(|&s| Claim::from(s))
        .filter(|c| c.id != 0)
        .collect();
    if claims.len() < input.len() {
        error!(
            "{} claim definitions did not make it into a claim",
            input.len() - claims.len()
        );
    }
    if log_enabled!(Level::Debug) {
        for clm in &claims {
            debug!("{:?}", clm);
        }
    }
    claims
}

/// Count the points that are covered by more then 1 claim
fn count_double_points(claims: &Claims) -> usize {
    let mut overlaps: PointSet = HashSet::new();
    for cl1 in 0..claims.len() {
        for cl2 in (cl1 + 1)..claims.len() {
            if let Some(overlap) = claims[cl1].overlap(&claims[cl2]) {
                for pnt in &overlap {
                    overlaps.insert(pnt);
                }
            }
        }
    }
    overlaps.len()
}

/// find a claim that has no overlap to ant other claim
fn find_unique_claims(claims: &Claims) -> Option<usize> {
    let mut has_overlap: Vec<bool> = (0..claims.len()).map(|_| false).collect();
    for cl1 in 0..claims.len() {
        for cl2 in (cl1 + 1)..claims.len() {
            if cl1 == cl2 {
                continue;
            }
            if claims[cl1].overlap(&claims[cl2]).is_some() {
                has_overlap[cl1] = true;
                has_overlap[cl2] = true;
            }
        }
    }
    debug!(
        "non-overlaps:  {}",
        has_overlap
            .iter()
            .enumerate()
            .filter(|(_, &b)| !b)
            .map(|(n, b)| format!("{}:{}:{}, ", n, b, claims[n].id))
            .collect::<String>()
    );
    let idx = has_overlap.iter().position(|v| !v);
    idx.map(|idx| claims[idx].id)
}

const CLAIMDEF: &str = r#"#(\d+)\s+@\s+(\d+),(\d+):\s+(\d+)x(\d+)"#;

impl Claim {
    /// Find the Area that is the overlap between self and another claim, if any
    fn overlap(&self, other: &Claim) -> Option<Area> {
        let tlx = self.tl.x.max(other.tl.x);
        let brx = self.br.x.min(other.br.x);
        if tlx > brx {
            return None;
        }

        let tly = self.tl.y.max(other.tl.y);
        let bry = self.br.y.min(other.br.y);
        if tly > bry {
            return None;
        }

        Some(Area {
            tl: Point { x: tlx, y: tly },
            br: Point { x: brx, y: bry },
        })
    }
}

impl From<&str> for Claim {
    /// Create a claim froma a claim definition
    fn from(s: &str) -> Self {
        lazy_static! {
            static ref RE: Regex = Regex::new(CLAIMDEF).unwrap();
        }
        if let Some(caps) = RE.captures(s) {
            let nums: Vec<usize> = caps
                .iter()
                .skip(1)
                .map(|cp| cp.unwrap().as_str().parse::<usize>().unwrap())
                .collect();
            let id = nums[0];
            let l = nums[1];
            let t = nums[2];
            let w = nums[3];
            let h = nums[4];
            Self {
                id,
                tl: Point { x: l, y: t },
                br: Point {
                    x: l + w - 1,
                    y: t + h - 1,
                },
            }
        } else {
            Claim::default()
        }
    }
}

/// Allow Area to be iterated over: for point in area {}
impl IntoIterator for &Area {
    type Item = Point;
    type IntoIter = AreaIterator;

    fn into_iter(self) -> Self::IntoIter {
        AreaIterator {
            area: self.clone(),
            x: self.tl.x,
            y: self.tl.y,
        }
    }
}

struct AreaIterator {
    area: Area, // Area to iterate over
    x: usize,   // x (from left) of next point to return
    y: usize,   // y (from top) of next point to return
}

impl Iterator for AreaIterator {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        if self.y > self.area.br.y {
            None
        } else {
            let item = Point {
                x: self.x,
                y: self.y,
            };
            self.x += 1;
            if self.x > self.area.br.x {
                self.x = self.area.tl.x;
                self.y += 1;
            }
            Some(item)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    #1 @ 1,3: 4x4
    #2 @ 3,1: 4x4
    #3 @ 5,5: 2x2
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(4, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().collect();
        assert_eq!(3, part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
