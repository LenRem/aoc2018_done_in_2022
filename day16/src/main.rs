// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input16.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        //.filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let (_, samples) = Sample::get_many(&input);
    samples
        .iter()
        .map(|s| score1(s))
        .filter(|score| *score >= 3)
        .count()
}

/// Make vectors of samples where n instructions are satisfied.
/// For each n:
///  - the length is one: link the code to the instruction.
///  - for larger n: take out the known instructions. See if 1 is left
fn part2(input: &[&str]) -> i32 {
    let mut opc2opr: Vec<Option<Operation>> = vec![None; Operation::iter().count()];    // map index to an opcode. If yet unknown: holds None
    let (end_of_samples, samples) = Sample::get_many(&input);   // Get all samples and the line offset where they end
    let oprs_for_sample: Vec<_> = samples.iter().map(|s| get_valid_operations(s)).collect();
    let max_opr_in_sample = oprs_for_sample
        .iter()
        .map(|opcds| opcds.len())
        .max()
        .unwrap();
    debug!("Max opc count for 1 sample: {max_opr_in_sample}");
    let mut round = 0;
    while opc2opr.iter().any(|oper| oper.is_none()) {
        // We have not figured them all out yet
        // Find which oprs_for_sample have a length 1, after taking off the known operations
        let mut added1 = false;
        for (n, opers) in oprs_for_sample.iter().enumerate() {    // for each sample
            debug!("Sample {}: {:?}", n, opers);
            let relevant_opers: Vec<_> = opers.iter().filter(|&opr| !opc2opr.contains(&Some(*opr))).clone().collect();
            debug!("Relevant: {:?}", relevant_opers);
            if relevant_opers.len() == 1 {
                let candidate_operation = *relevant_opers[0];
                let candidate_opcode = samples[n].instr[0] as usize;
                match opc2opr[candidate_opcode] {
                    None => {
                        opc2opr[candidate_opcode] = Some(candidate_operation);
                        added1 = true;
                    },
                    Some(opr) => if opr != candidate_operation {
                        error!("In sample {}: found operation {:?} for opcode {}, while expecting to resolve {} to {:?}", 
                                n, opc2opr[candidate_opcode], candidate_opcode, candidate_opcode, candidate_operation );
                    }
                }
            }
        }
        debug!("Round {}: {:?}", round, opc2opr);
        if !added1 {
            error!("Could not assign a new operation");
            return 0
        }  
        round += 1;
        if round > 10 {
            return 0;
        }
        
    }
    // Up here we have a compelety populated opc2opr. Let's get rid of the Options.
    let opc2opr: Vec<_> = opc2opr.iter().map(|e| e.unwrap()).collect();
    let mut cpu = Cpu::new(opc2opr);
    for &line in &input[end_of_samples..]{
        if line.trim().is_empty() {
            continue
        }
        let instr = instr_new(line).unwrap();
        cpu.exec(&instr);
    }
    cpu.regs[0]
}

type Regs = [i32; 4];

#[derive(Debug)]
struct Sample {
    before: Regs,
    instr: [i32; 4],
    after: Regs,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Operation {
    ADDR, // rC := rA + rB
    ADDI, // rC := rA + vB
    MULR, // rC := rA * rB
    MULI, // rC := rA * vB
    BANR, // rC := rA & rB
    BANI, // rC := rA & vB
    BORR, // rC := rA | rB
    BORI, // rC := rA | vB
    SETR, // rC := rA     // ignore rB
    SETI, // rC := vA     // ignroe rB
    GTIR, // rC := vA > rB
    GTRI, // rC := rA > vB
    GTRR, // rC := rA > rB
    EQIR, // rC := vA == rB
    EQRI, // rC := rA == vB
    EQRR, // rC := rA == rB
}

struct Cpu {
    regs: Regs,
    opc2opr: Vec<Operation>,
}

/// reg_new() takes a string as input that is formatted like so:
/// "[a, b, c, d]",  where a, b, c and d are integers"
/// This string reflects a snapshot of register values of the Cpu
fn reg_new(inp: &str) -> Option<Regs> {
    if let Some(ln) = inp.strip_prefix("[") {
        if let Some(ln) = ln.strip_suffix("]") {
            let mut n_iter = ln
                .split(", ")
                .map(|nstr| nstr.parse::<i32>().unwrap())
                .enumerate();
            let mut result = [0, 0, 0, 0];
            while let Some((n, v)) = n_iter.next() {
                result[n] = v;
            }
            return Some(result);
        }
    }
    None
}

/// instr_new takes a string as input that is formatted like so:
/// "a, b, c, d",  where a, b, c and d are integers"
/// This string reflects a command that the cpu can execute
fn instr_new(inp: &str) -> Option<[i32; 4]> {
    let mut n_iter = inp
        .split(" ")
        .map(|nstr| nstr.parse::<i32>().unwrap())
        .enumerate();
    let mut result = [0, 0, 0, 0];
    while let Some((n, v)) = n_iter.next() {
        result[n] = v;
    }
    return Some(result);
}

fn score1(sample: &Sample) -> usize {
    get_valid_operations(sample).len()
}

/// For the given sample, find which operations could satisfy the command of the sample.  
/// The result is a list of possibly valid operations
fn get_valid_operations(sample: &Sample) -> Vec<Operation> {
    let mut valid_opcs = vec![];
    for opc in Operation::iter() {
        let result = opc.exec(&sample.instr, &sample.before);
        if result == sample.after {
            valid_opcs.push(*opc);
        }
    }
    valid_opcs
}

impl Sample {
    /// get_many reads a number of samples from input and constructs a vector of Sample from them
    /// Samples are separated from each other by 1 empty line
    /// More then 1 empty line in a row signals the end of the samples
    /// The number of lines describing the samples, and the samples, are returned
    fn get_many(inp: &[&str]) -> (usize, Vec<Sample>) {
        let mut inp = inp;
        let mut end = 3;
        let empties: Vec<_> = inp
            .iter()
            .enumerate()
            .filter(|(_, ln)| ln.is_empty())
            .map(|(n, _)| n)
            .collect();
        if empties.len() >= 3 {
            let start = inp.iter().position(|&line| !line.is_empty()).unwrap();
            for (n, chnk) in inp.windows(3).enumerate() {
                if chnk.iter().all(|s| s.is_empty()) {
                    end = n;
                    break;
                }
            }
            // debug!("End at {end}");
            inp = &inp[start..end];
        }
        (end, inp.chunks(4).map(|chnk| Sample::new(chnk)).collect())
    }

    /// new constructs a sample from 3 lines of text. A 4th empty line may be present
    fn new(inp: &[&str]) -> Self {
        let before = reg_new(inp[0].strip_prefix("Before: ").unwrap()).unwrap();
        let after = reg_new(inp[2].strip_prefix("After:  ").unwrap()).unwrap();
        let instr = instr_new(inp[1]).unwrap();
        Sample {
            before,
            instr,
            after,
        }
    }
}

impl Operation {
    fn exec(&self, cmd: &[i32; 4], regs: &Regs) -> Regs {
        let mut outregs = *regs;
        let output = &mut outregs[cmd[3] as usize];
        use Operation::*;
        match self {
            ADDR => *output = regs[cmd[1] as usize] + regs[cmd[2] as usize],
            ADDI => *output = regs[cmd[1] as usize] + cmd[2],
            MULR => *output = regs[cmd[1] as usize] * regs[cmd[2] as usize],
            MULI => *output = regs[cmd[1] as usize] * cmd[2],
            BANR => *output = regs[cmd[1] as usize] & regs[cmd[2] as usize],
            BANI => *output = regs[cmd[1] as usize] & cmd[2],
            BORR => *output = regs[cmd[1] as usize] | regs[cmd[2] as usize],
            BORI => *output = regs[cmd[1] as usize] | cmd[2],
            SETR => *output = regs[cmd[1] as usize],
            SETI => *output = cmd[1],
            GTIR => *output = if cmd[1] > regs[cmd[2] as usize] { 1 } else { 0 },
            GTRI => *output = if regs[cmd[1] as usize] > cmd[2] { 1 } else { 0 },
            GTRR => {
                *output = if regs[cmd[1] as usize] > regs[cmd[2] as usize] {
                    1
                } else {
                    0
                }
            }
            EQIR => {
                *output = if cmd[1] == regs[cmd[2] as usize] {
                    1
                } else {
                    0
                }
            }
            EQRI => {
                *output = if regs[cmd[1] as usize] == cmd[2] {
                    1
                } else {
                    0
                }
            }
            EQRR => {
                *output = if regs[cmd[1] as usize] == regs[cmd[2] as usize] {
                    1
                } else {
                    0
                }
            }
        }
        // debug!("Executing");
        outregs
    }

    fn iter() -> impl Iterator<Item = &'static Operation> {
        use Operation::*;
        [
            ADDR, ADDI, MULR, MULI, BANR, BANI, BORR, BORI, SETR, SETI, GTIR, GTRI, GTRR, EQIR,
            EQRI, EQRR,
        ]
        .iter()
    }
}

impl Cpu {
    fn new(opc2opr: Vec<Operation> ) -> Self {
        Self { 
            regs: [0;4],
            opc2opr,
        }
    }

    fn exec(&mut self, cmd: &[i32;4]) {
        self.regs = self.opc2opr[cmd[0] as usize].exec( cmd, &self.regs)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
 
    const INPUT01: &str = "\
    Before: [3, 2, 1, 1]
    9 2 1 2
    After:  [3, 2, 2, 1]
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            //.filter(|s| !s.is_empty())
            .collect();
        assert_eq!(1, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(1, part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
