// Needed use statements for scaffolding
use std::fmt::Display;
use std::fmt::Error;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::VecDeque; // {HashSet, HashMap, VecDeque};
use std::ops::RangeInclusive;

const INPUTTXT: &str = include_str!("../input12.txt");

// const LOGLEVEL: LevelFilter = LevelFilter::Info;
const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

const ROUNDS: usize = 110;

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> i64 {
    let (mut pots, notes) = read_data(input);
    debug!("Notes:\n{}", notes2string(&notes));
    debug!("{:2}: {}", 0, pots);
    for n in 0..ROUNDS {
        pots.generate(&notes);
        let score = pots.score1();
        println!("{:2}:{:10}: {}", n + 1, score, pots);
    }
    println!("{} ronds", ROUNDS);
    pots.score1()
}

///Part 1 with more rounds shows that the pattern stabilizes and moves to he right.
/// The score is predictable after 100 rounds and is 1175 + 50*N, N >= 100.
fn part2(_input: &[&str]) -> usize {
    let rounds = 50_000_000_000;
    println!("{} ronds", rounds);
    1175 + rounds * 50
}

#[derive(Debug)]
struct Pots {
    pots: Vec<bool>, // The row of pots, containing a plant (true) or not
    zero: usize,     // pots[zero] is pot nr 0. pots[0] = pot nr -offset
}

type Notes = Vec<bool>;

// read_data reads the initial state and the series of notes about the spread of plants
// The initial state is stored in a Pots struct, where offset denotes the index into the pots row
// where pot 0 is described, by lack of negative indexes.
// The spread notes is a vector of 32, where each index is the number formed by the bits of the pots recipy,
// the stored bool denoting the spread result.
fn read_data(input: &[&str]) -> (Pots, Notes) {
    const INITIAL_STATE: &str = "initial state: ";

    let mut init_state_seen = false;
    let mut potv = Vec::new();
    let mut notes = vec![false; 32];
    for &line in input.into_iter() {
        if !init_state_seen {
            if line.starts_with(INITIAL_STATE) {
                let initial_state = &line[INITIAL_STATE.len()..];
                potv = initial_state.chars().map(|c| c == '#').collect();
                init_state_seen = true;
            }
        } else {
            let parts: Vec<_> = line.split(" => ").collect();
            let index: usize = parts[0].chars().enumerate().fold(0_usize, |acc, (n, c)| {
                if c == '#' {
                    acc + (1 << 4 - n)
                } else {
                    acc
                }
            });
            let value = parts[1] == "#";
            notes[index] = value;
        }
    }
    (
        Pots {
            pots: potv,
            zero: 0,
        },
        notes,
    )
}

fn notes2string(notes: &Notes) -> String {
    notes
        .iter()
        .enumerate()
        .map(|(n, b)| format!(" {:2}:{}", n, if *b { '#' } else { '.' }))
        .collect::<Vec<String>>()
        .join("\n")
}

impl Pots {
    /// range() returns an inclusive range just covering the first and the last position containing a plant, disregarding offset
    /// For the real pot labels, substract offset (possibly ging negative!)
    fn range(&self) -> RangeInclusive<usize> {
        let mut min = usize::MAX;
        let mut max = usize::MIN;
        self.pots
            .iter()
            .enumerate()
            .filter(|(_, &b)| b)
            .for_each(|(n, _)| {
                min = min.min(n);
                max = n;
            });
        min..=max
    }

    /// Given a mid position n, consider the pots[n-2..=n+2] and construct a number
    /// according to the bitpattern of those pots being loaded
    /// panics if mid < 2 or mid > pots.len() - 2
    fn notes_index(&self, mid: usize) -> usize {
        self.pots[(mid - 2)..=(mid + 2)]
            .iter()
            .enumerate()
            .filter(|(_, &b)| b)
            .fold(0, |acc, (n, _)| acc + (1 << 4 - n))
    }

    /// generate() yields the next cycle of plants in pots
    /// It is ensure that at least  empty pots are included at both sides of the range.
    /// Then all pots except the first and last 2 are considered
    fn generate(&mut self, notes: &Notes) {
        let mut minmax = self.range();

        // ensure there are at last 2 pots before the first loaded one
        let extra = if minmax.start() > &4 {
            0
        } else {
            4 - minmax.start()
        };
        for _ in 0..extra {
            self.pots.insert(0, false);
        }
        self.zero += extra;
        minmax = minmax.start() + extra..=(minmax.end() + extra);

        // ensure there are at least 2 pots after the last loaded one
        let extra = if *minmax.end() > self.pots.len() - 5 {
            minmax.end() + 5 - self.pots.len()
        } else {
            0
        };
        for _ in 0..extra {
            self.pots.push(false);
        }
        let mut new_pots: Vec<bool> = vec![false; self.pots.len()];
        for n in 2..self.pots.len() - 2 {
            let index = self.notes_index(n);
            new_pots[n] = notes[index];
        }
        self.pots = new_pots;
    }

    /// score adds the values on the labels of all pots with plants
    fn score1(&self) -> i64 {
        self.pots
            .iter()
            .enumerate()
            .filter(|(_, &b)| b)
            .map(|(n, _)| n as i64 - self.zero as i64)
            .sum()
    }
}

impl Display for Pots {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), Error> {
        let string1: String = self
            .pots
            .iter()
            .enumerate()
            .map(|(n, &b)| {
                if n != self.zero {
                    if b {
                        '#'
                    } else {
                        '.'
                    }
                } else {
                    if b {
                        '%'
                    } else {
                        '_'
                    }
                }
            })
            .collect();
        write!(f, "{}", string1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    initial state: #..#.#..##......###...###

    ...## => #
    ..#.. => #
    .#... => #
    .#.#. => #
    .#.## => #
    .##.. => #
    .#### => #
    #.#.# => #
    #.### => #
    ##.#. => #
    ##.## => #
    ###.. => #
    ###.# => #
    ####. => #
";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(325, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(1, part2(&input));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
