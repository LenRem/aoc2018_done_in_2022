// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

// use std::collections::{HashSet, HashMap, VecDeque};

use regex::Regex;
use lazy_static::lazy_static;

const INPUTTXT: &str = include_str!("../input23.txt");

fn main() {
    let env = env_logger::Env::default();
    env_logger::Builder::from_env(env)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().map(|s|s.trim()).filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let bots: Bots = input.iter().map(|line| Bot::new(line)).collect();
    let maxrad = bots.iter().map(|b| b.r).max().unwrap();
    let bot0 = bots.iter().find(|b| b.r == maxrad ).unwrap();
    bots.iter().filter(|b| bot0.p.dist(&b.p) <= maxrad).count()
}

/// Just trying all points is a no go: just too many to try.
/// For eah bot, determine the min and ma distance of its range to the origin.
/// Put these in sequence of found distances
/// Count from the smallest distance: +1 if it is a minimum distance, -1 if it is a mzximum distance.
/// Each time the count reaches a mazimum (can only happen on a min distance: +1), record the new max count and the causing distance.
/// At the end, the causing distance is the answer.
/// Shamelessly derived from a post on https://www.reddit.com/r/adventofcode/comments/a8s17l/2018_day_23_solutions by https://www.reddit.com/user/EriiKKo/
/// 
fn part2(input: &[&str]) -> i64 {
    let bots: Bots = input.iter().map(|line| Bot::new(line)).collect();
    let mut max_count = 0;
    let mut result = 0;
    let mut count = 0;
    let mut que = Vec::with_capacity(2 * bots.len());
    for bot in bots {
        let d = bot.p.modulus();
        que.push(((d-bot.r).max(0), 1));
        que.push((d+bot.r+1, -1));
    }
    que.sort();
    for q in que {
        count += q.1;
        if count > max_count {
            max_count = count; 
            result = q.0;
        }
    }
    result
}

type Radius = i64;
type Bots = Vec<Bot>;

#[derive(Debug)]
struct Bot {
    p: Pnt3,
    r: Radius,
}

#[derive(Debug)]
struct Pnt3 {
    x: i64,
    y: i64, 
    z: i64,
}

const BOTDEF: &str  = r#"pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)"#;

impl Bot {
    /// new() takes a well formed string and makes a Bot out of it.
    /// Panics if the string is not wel formed.
    fn new(inp: &str) -> Bot {
        lazy_static! {
            static ref RE: Regex = Regex::new(BOTDEF).unwrap();
        }
        let mut bot = Bot{ p: Pnt3::new(0,0,0), r: 0 };
        if let Some(caps) = RE.captures(inp){
            bot.p.x = caps.get(1).map(|m| m.as_str().parse::<i64>().unwrap()).unwrap();
            bot.p.y = caps.get(2).map(|m| m.as_str().parse::<i64>().unwrap()).unwrap();
            bot.p.z = caps.get(3).map(|m| m.as_str().parse::<i64>().unwrap()).unwrap();
            bot.r = caps.get(4).map(|m| m.as_str().parse::<i64>().unwrap()).unwrap();
        }
        bot
    }
}

impl Pnt3 {
    fn new(x: i64, y: i64, z: i64 ) ->  Pnt3 {
        Pnt3 { x, y, z }
    }

    fn modulus(&self ) -> i64 {
        self.x.abs() + self.y.abs() + self.z.abs()
    }

    fn dist(&self, other: &Self)  -> i64 {
        (self.x - other.x).abs() + (self.y - other.y).abs() + (self.z - other.z).abs()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    pos=<0,0,0>, r=4
    pos=<1,0,0>, r=1
    pos=<4,0,0>, r=3
    pos=<0,2,0>, r=1
    pos=<0,5,0>, r=3
    pos=<0,0,3>, r=1
    pos=<1,1,1>, r=1
    pos=<1,1,2>, r=1
    pos=<1,3,1>, r=1
            ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(7, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(1, part2(&input));
    }

    fn init_logger() {
        let env = env_logger::Env::default();
        env_logger::Builder::from_env(env)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init().ok();
    }
}
