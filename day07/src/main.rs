// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::{HashMap, HashSet}; //{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input07.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> String {
    let reqmap = get_requirements(input);
    trace!("ReqMap: {:?}", &reqmap);
    let mut result: Vec<Step> = Vec::with_capacity(reqmap.len());
    loop {
        let made: StepSet = result.iter().map(|c| *c).collect();
        let available = find_available(&reqmap, &made);
        debug!("avl: {:?} --> {:?} ", made, available);
        if available.is_empty() {
            break;
        }
        let lowest = *available.iter().min().unwrap();
        debug!("Now making {}", lowest);
        result.push(*available.iter().min().unwrap());
    }
    result.iter().collect()
}

fn part2(input: &[&str]) -> Time {
    part2_2(input, 60, 5)
}

fn part2_2(input: &[&str], time_adder: Time, workers: usize) -> Time {
    let reqmap = get_requirements(input);
    let mut result: Vec<Step> = Vec::with_capacity(reqmap.len());
    let mut time = 0_usize;
    let mut pipeline: HashMap<Step, Time> = HashMap::with_capacity(workers);
    loop {
        let made: StepSet = result.iter().map(|c| *c).collect();
        let mut available = find_available(&reqmap, &made);
        debug!("avl: {:?} --> {:?} ", made, available);
        // Check all availables into the pipeline if not already there, with the time of completion
        while !available.is_empty() && pipeline.len() < workers {
            let lowest = *available.iter().min().unwrap();
            pipeline
                .entry(lowest) // Only insert if not inserted yet
                .or_insert(time + time_adder + 1 + (lowest as u8 - 'A' as u8) as usize);
            available.remove(&lowest);
        }
        if available.is_empty() && pipeline.is_empty() {
            return time;
        }
        debug!("Being assembled: {:?}", pipeline);

        time = *pipeline.values().min().unwrap(); // Advance time to when a step is completed
        let make_now: Vec<_> = pipeline
            .iter()
            .filter(|(_, &t)| t == time)
            .map(|(c, _)| *c)
            .collect();
        for s in make_now {
            result.push(s);
            debug!("Step '{}' ready at time {}", s, time);
            pipeline.remove(&s);
        }
    }
}

type Step = char;
type Time = usize;
type StepSet = HashSet<Step>;
type ReqMap = HashMap<Step, StepSet>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Fabr {
    ready: Time,
    item: Step,
}

// Get a ReqMap from the input instructions.
fn get_requirements(input: &[&str]) -> ReqMap {
    let mut reqmap = ReqMap::new();
    for line in input {
        let line = line.trim_start_matches("Step ");
        let req = line.chars().nth(0).unwrap();
        let line = line[1..].trim_start_matches(" must be finished before step ");
        let step = line.chars().nth(0).unwrap();
        // Insert requirements as well, in case they have none themselves
        reqmap.entry(req).or_insert(HashSet::new());
        let value = reqmap.entry(step).or_insert(HashSet::new());
        value.insert(req);
    }
    reqmap
}

// Find a set of steps becoming available based on the avl StepSet
fn find_available(reqmap: &ReqMap, avl: &StepSet) -> StepSet {
    reqmap
        .iter()
        .filter(|(c, _)| !avl.contains(c))
        .inspect(|e| trace!("Testing {}, needing {:?}", e.0, e.1))
        .filter(|(_stp, req)| req.iter().all(|rq| avl.contains(rq)))
        .inspect(|_| trace!("OK"))
        .map(|(stp, _)| *stp)
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    Step C must be finished before step A can begin.
    Step C must be finished before step F can begin.
    Step A must be finished before step B can begin.
    Step A must be finished before step D can begin.
    Step B must be finished before step E can begin.
    Step D must be finished before step E can begin.
    Step F must be finished before step E can begin.
    ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!("CABDFE", part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(15, part2_2(&input, 0, 2));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
