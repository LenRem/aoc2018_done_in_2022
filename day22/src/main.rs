// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::{HashMap, HashSet};  //{HashSet, HashMap, VecDeque};
use std::convert::From;

const INPUTTXT: &str = include_str!("../input22.txt");

fn main() {
    let env = env_logger::Env::default();
    env_logger::Builder::from_env(env)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT.lines().map(|s|s.trim()).filter(|s| !s.is_empty()).collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let depth = input[0].split_once(": ").unwrap().1.parse::<usize>().unwrap();
    let targetstr = input[1].split_once(": ").unwrap().1.trim();
    let target = Pnt::from_str(targetstr);
    let cave = Cave::new(depth, target);
    let risklvl = cave.risk_level();
    debug!("Cave:\n{}\n has risk level {}", cave.as_string1(), risklvl);
    // debug!("Cave has risk level {}", risklvl);
    risklvl
}

/// Part 1 allowed a fixed cave size reaching up to T
/// Part 2 needs an expandable cave, including T and extending past T.
/// So just saving all properties in a vector won't do: if the cave expands, the entire vector would have to be reshuffled.
/// So we will switch to HashMaps, where the key is a Pnt, adnd the value is the GeoIndex or EroLvl
/// Each time we enter a coordinate with an unvisited Y or X, we will first calculate the values for the entire Y-column or X-row.
/// 
/// To combine all this with part1, we will leave the part 1 structs and functions as is, except we add to more storages to Cave
fn part2(input: &[&str]) -> usize {
    let depth = input[0].split_once(": ").unwrap().1.parse::<usize>().unwrap();
    let targetstr = input[1].split_once(": ").unwrap().1.trim();
    let target = Pnt::from_str(targetstr);
    let mut cave = Cave::new(depth, target);
    let result = cave.search_len();
    debug!("Cave according to 2: \n{}", cave.as_string2());
    result
}

const GEO_Y0: usize = 16807;
const GEO_X0: usize = 48271;
const MOD_G2E: usize = 20183;

type Id = usize;
type GeoIndex = usize;
type VecGeo = Vec<GeoIndex>;
type MapGeo = HashMap<Pnt, GeoIndex>;
type EroLvl = usize;
type VecEro = Vec<EroLvl>;
type MapEro = HashMap<Pnt, EroLvl>;

struct Cave {
    target: Pnt,
    depth: usize,
    geo: VecGeo,
    ero: VecEro,
    geo2: MapGeo,
    ero2: MapEro,
    nxt_x: usize,
    nxt_y: usize,
}

enum Area {
    Rocky = 0,
    Wet,
    Narrow,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Pnt {
    x: usize,
    y: usize,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
enum Tool {
    Neither,
    Torch,
    ClimbGr,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
/// A node in the graph to search
struct Node {
    p: Pnt,
    t: Tool,
}

enum Dir {
    N,
    E,
    S,
    W,
}

impl Cave {
    fn new(depth: usize, target: Pnt ) -> Cave {
        let mut cave = Cave {
            target,
            depth,
            geo: vec![],
            ero: vec![],
            geo2: MapGeo::new(),
            ero2: MapEro::new(),
            nxt_x: 1,
            nxt_y: 1,
        };
        cave.make_values1();
        let m = Pnt::new(0,0);
        cave.geo2.insert(m, 0);
        cave.ero2.insert(m, cave.g2e(0));
        cave
    }

    // Translate an x and y  to an Id
    // x and y can at most be the target coordinates.
    fn xy2i(&self, x: usize, y: usize) -> Id {
        y * (self.target.x+1) + x
    }

    /// Convert a GeoIndex into an EroLvl
    fn g2e(&self, g: GeoIndex ) -> EroLvl {
        (g + self.depth) % MOD_G2E
    }

    /// Make all geological indices and erosion levels for part1
    /// This must be done from the x==0 side and the y==0 side onwards, so that perviously derived erosion levels
    /// van be used to calculate geometric levels on the inner regions of the cave
    fn make_values1(&mut self) {
        let sz = (self.target.x+1) * (self.target.y+1);
        let mut geo = vec![0;sz];
        let mut ero = vec![0;sz];
        for orgn in 0..=self.target.x.min(self.target.y) {
            trace!("orig {orgn}");
            for x in orgn..=self.target.x {
                // y is orgn
                let id = self.xy2i(x, orgn); 
                geo[id] = if orgn == 0 { x * GEO_Y0 } else { ero[self.xy2i(x-1, orgn)] * ero[self.xy2i(x, orgn-1)] };
                ero[id] = self.g2e(geo[id]);
            }
            for y in orgn..=self.target.y {
                // x is orgn
                let id = self.xy2i(orgn, y);
                geo[id] = if orgn == 0 { y * GEO_X0 }  else { ero[self.xy2i(orgn-1, y)] * ero[self.xy2i( orgn, y-1)] };
                ero[id] = self.g2e(geo[id]);
            }
        }
        // Location of target
        geo[sz-1] = 0;        
        ero[sz-1] = self.g2e(geo[sz-1]);

        // update cave
        self.geo = geo;
        self.ero = ero;
    }

    /// Part2:
    /// Calculate GeoIndex and EroLvl for the column with the indicated x
    /// The column with x-1 must have been calculated before, or a Result::Err be returned.
    fn add_x(&mut self, x: usize) -> Result<()> {
        if x < self.nxt_x {
            return Ok(());      // Already done
        }
        if x > self.nxt_x {
            bail!("Must have column for x = {} before attempting to add columns x = {}", self.nxt_x, x);
        }
        // x == nxt_x
        if x == 0 {
            for y in 0..self.nxt_y {
                let pnt = Pnt{ x: 0, y };                
                self.geo2.insert(pnt,y * GEO_X0);
                self.ero2.insert(pnt, self.g2e(self.geo2[&pnt]));  
            }
        } else { 
            for y in 0..self.nxt_y {
                let pnt = Pnt{ x, y };
                self.geo2.insert(pnt, if pnt == self.target { 
                    0 
                } else if y == 0 {
                    x * GEO_Y0 
                } else { 
                    let left = Pnt{x, y:y-1};
                    let up = Pnt{ x: x-1, y};
                    self.ero2[&left] * self.ero2[&up] 
                });
                self.ero2.insert(pnt, self.g2e(self.geo2[&pnt]));  
            }
        }
        self.nxt_x = x+1;
        Ok(())
    }

    /// Part2:
    /// Calculate GeoIndex and EroLvl for the row with the indicated y
    /// The column with y-1 must have been calculated before, or a Result::Err be returned.
    fn add_y(&mut self, y: usize) -> Result<()> {
        if y < self.nxt_y {
            return Ok(());      // Already done
        }
        if y > self.nxt_y {
            bail!("Must have row for y = {} before attempting to add row for y = {}", self.nxt_y, y);
        }
        // y == nxt_y
        if y == 0 {
            for x in 0..self.nxt_x {
                let pnt = Pnt{ x: 0, y };                
                self.geo2.insert(Pnt{ x, y: 0 }, x * GEO_Y0);
                self.ero2.insert(pnt, self.g2e(self.geo2[&pnt]));  
            }
        } else { 
            for x in 0..self.nxt_x {
                let pnt = Pnt{ x, y };
                self.geo2.insert(pnt, if pnt ==  self.target {
                    0
                } else if x == 0 {
                    y * GEO_X0 
                }  else { 
                    let left = Pnt{x, y:y-1};
                    let up = Pnt{ x: x-1, y};
                    self.ero2[&left] * self.ero2[&up] 
                });
                self.ero2.insert(pnt, self.g2e(self.geo2[&pnt]));
            }
        }
        self.nxt_y = y+1;
        Ok(())
    }

    fn risk_level(&self) -> usize {
        self.ero.iter().map(|e| e % 3).sum()
    }

    /// Part2:
    /// Return a vector of all reachable nodes
    /// 
    fn succ(&mut self, n: &Node) -> Vec<Node> {
        let mut result = vec![];
        // Same point, other tool
        let regtp  = Area::from(self.ero2[&n.p]);
        result.push( Node::new( n.p.x, n.p.y, regtp.other_tool(&n.t) ));
        // same tool accessible points
        use Dir::*;
        for dir in Dir::as_vec() {
            if let Some(np) = match dir {
                N => if n.p.y > 0 { Some(Pnt::new(n.p.x, n.p.y-1)) } else { None },
                E => Some(Pnt::new(n.p.x+1, n.p.y)),
                S => Some(Pnt::new(n.p.x, n.p.y+1)),
                W => if n.p.x > 0 { Some(Pnt::new(n.p.x-1, n.p.y)) } else  { None },
            } {
                // make sure np is on the map
                if np.x == self.nxt_x {
                    self.add_x(np.x).unwrap();
                }
                if np.y == self.nxt_y {
                    self.add_y(np.y).unwrap();
                }
                // See that the current tool matches the next point
                let na = Area::from(self.ero2[&np]);
                if na.tools().contains(&n.t) {
                    result.push( Node::new(np.x, np.y, n.t));
                } 
            }
        }
        result
    }                   

    fn as_string1(&self) -> String {
        let mut vc = Vec::with_capacity(self.target.y+1);
        for y in 0..=self.target.y {
            vc.push(self.ero[self.xy2i(0,y)..self.xy2i(0,y+1)].iter().map(|e| Area::from(*e).as_char()).collect::<String>());
        }
        vc[0].replace_range(0..1, "M");
        let pos_t = vc[0].len()-1;
        vc[self.target.y].replace_range(pos_t..pos_t+1, "T");
        vc.join("\n")
    }

    fn as_string2(&self) -> String {
        println!("Ero2 len: {}", self.ero2.len());
        for x in 0..=self.target.x {
            for y in 0..=self.target.y {
                if !self.ero2.contains_key( &Pnt{ x, y }) {
                    println!("Missing point ({}, {})", x, y);
                }
            }
        }
        let mut vc = Vec::with_capacity(self.target.y+1);
        for y in 0..=self.target.y {
            let mut row = String::with_capacity(self.target.x+1);
            for x in 0..=self.target.x {
                row.push(Area::from(self.ero2[&Pnt{x, y}]).as_char());
            }
            vc.push(row);
        }
        vc[0].replace_range(0..1, "M");
        let pos_t = vc[0].len()-1;
        vc[self.target.y].replace_range(pos_t..pos_t+1, "T");
        vc.join("\n")
    }

    /// Part2
    /// Find the time (path_length to get from (0,0) to target
    /// The search is a Dykstra kind of thing:
    /// -  the search is over coordinated x (x >= 0), y (y >= 0) and tool (3 values: Torch, ClimbG, Neither)
    /// -  the endpoint is Target with Torch
    /// -  the possible directions are bounded by sign of X and Y and the EroLvl of the neighbour being allowable for the current tool
    ///    as there are always 2 tools available for any region type, there are at most 5 ways to go from any point (4 directions or 1 other tool)
    /// -  the graph is unbounded, so we will need to work with a visited set and a seen set.
    /// -  once Target/Torch is visited, the search ends.

    fn search_len(&mut self) -> usize {
        #[derive(Copy, Clone)]
        struct Visited {
            len: usize, 
            pred: Node,
        }
        trace!("Entering search");
        let mut visited: HashMap<Node, Visited> = HashMap::new();
        let mut seen: HashMap<Node, Visited> = HashMap::new();
        let target = Node::new(self.target.x, self.target.y, Tool::Torch );
        let start = Node::new(0, 0, Tool::Torch );
        seen.insert( Node::new(0,0, Tool::Torch), Visited { len: 0, pred: Node::new(0,0, Tool::Torch) });
        while !seen.is_empty() {
            // Select next node
            let min_d = seen.iter().map(|(_,v)| v.len).min().unwrap();
            let node = seen.iter().filter(|(_, v)| v.len == min_d).map(|(&n,_)| n).collect::<Vec<_>>()[0];
            // Handle found node
            let data = seen.remove(&node).unwrap();
            trace!("Looking at node {}:{}", node, data.len);
            visited.insert( node, data);
            if node == target {
                break;
            }
            for nn in self.succ(&node) {
                if visited.contains_key(&nn) { continue } // node has been visited. Done
                let newlen = data.len + if nn.t == node.t  { 1 } else { 7 };
                if let Some(sv) = seen.get(&nn) {
                    if sv.len <= newlen { continue };       // Place already seen with a better or equal path
                }
                let nv = Visited { len: newlen, pred: node };
                seen.insert(nn, nv);
            }
        }
        trace!("Done searching. Report");
        // Find target in visited and return result
        // Report path
        let mut nd = target;
        let mut path = vec![nd];
        while let Some(&v) = visited.get(&nd) {
            trace!("At {} at distance {} with pred {}", nd, v.len, v.pred);
            if nd == start { break; }
            path.push(v.pred);
            nd = v.pred;
        }
        let pnts:  Vec<_> = path.iter().rev().map(|n| format!("{}", n)).collect();
        let pntsstr = pnts.join(" ");
        debug!("Target path: {}", pntsstr);
        visited.get(&target).unwrap().len
    }
}


impl Area {
    fn as_char(&self) -> char {
        match self {
            Area::Rocky => '.',
            Area::Wet => '=',
            Area::Narrow => '|',
        }
    }

    /// Returns the tool that can be used on the area
    fn tools(&self) -> [Tool;2] {
        match self {
            Area::Rocky => [Tool::Torch, Tool::ClimbGr],
            Area::Wet => [Tool::Neither, Tool::ClimbGr],
            Area::Narrow => [Tool::Neither, Tool::Torch],
        }
    }
     
    /// Given the current Area and the current tool, 
    /// return the other tool that would fit this area
    fn other_tool(&self, tool: &Tool) -> Tool {
        let tools = self.tools();
        assert!(tools.contains(tool));
        if tools[0] == *tool {
            tools[1] 
        } else {
            tools[0]
        }
    }
}

impl Pnt {
    fn from_str(inp: &str) -> Pnt {
        let (xstr, ystr) = inp.split_once(",").unwrap();
        let x = xstr.trim().parse().unwrap();
        let y = ystr.trim().parse().unwrap();
        Pnt { x, y }
    }

    fn new(x: usize, y: usize) -> Pnt {
        Pnt { x, y }
    }
}

impl Node {
    fn new(x: usize, y: usize, t: Tool ) -> Node {
        Node { p: Pnt{ x, y }, t }
    }
}

impl Dir {
    fn as_vec() -> [Dir;4] {
        use Dir::*;
        return [N, E, S, W];
    }
}

impl From<EroLvl>  for Area {
    fn from(inp: EroLvl) -> Area {
        match inp % 3 {
            0 => Area::Rocky,
            1 => Area::Wet,
            _ => Area::Narrow,
        }
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({},{})/{:?}", self.p.x, self.p.y, self.t)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    depth: 510
    target: 10,10
        ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(1, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01.lines().map(|s| s.trim()).filter(|s|!s.is_empty()).collect();
        assert_eq!(45, part2(&input));
    }

    fn init_logger() {
        let env = env_logger::Env::default();
        env_logger::Builder::from_env(env)
                .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init().ok();
    }
}
