// Needed use statements for scaffolding
use std::io::Write;
use std::time::Instant;
use std::{default, fmt::Display};

// use anyhow::{bail, Context, Result};
use log::{debug, error, info, log_enabled, trace, Level, LevelFilter};

use std::collections::HashSet; //{HashSet, HashMap, VecDeque};

const INPUTTXT: &str = include_str!("../input06.txt");

const LOGLEVEL: LevelFilter = LevelFilter::Info;
// const LOGLEVEL: LevelFilter = LevelFilter::Debug;
// const LOGLEVEL: LevelFilter = LevelFilter::Trace;

fn main() {
    env_logger::Builder::new()
        .filter_level(LOGLEVEL)
        .format(|buf, record| {
            writeln!(
                buf,
                "{:6} [{}]: {}",
                record.line().unwrap(),
                record.level(),
                record.args()
            )
        })
        .init();

    let input: Vec<&str> = INPUTTXT
        .lines()
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();
    run_and_report("Part1", &part1, &input);
    run_and_report("Part2", &part2, &input)
}

fn run_and_report<R: Display>(name: &str, f: &dyn Fn(&[&str]) -> R, input: &[&str]) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &[&str]) -> usize {
    let mut map = make_map(input);
    let (tl, br) = map_size(&map);
    info!("tl: {:?}, br: {:?}", tl, br);
    debug!("Map:\n{}", map_to_string(&map, &br));
    assign_points(&mut map, &tl, &br);
    let area_sizes: Vec<_> = map.iter().map(|loc| (loc.pnt, loc.size)).collect();
    if log_enabled!(Level::Debug) {
        for (n, (pnt, area)) in area_sizes.iter().enumerate() {
            debug!("Area {}:{:?}: size: {:?}", n, pnt, area);
        }
    }
    area_sizes
        .iter()
        .map(|(_, area)| {
            if let AreaSz::Finite(sz) = area {
                *sz
            } else {
                0
            }
        })
        .max()
        .unwrap() as usize
}

fn part2(input: &[&str]) -> usize {
    part2_2(input, 10000)
}

fn part2_2(input: &[&str], maxdist: usize) -> usize {
    let map = make_map(input);
    let (tl, br) = map_size(&map);
    let distances = dist_to_all(&map, &br);
    distances.iter().flatten().filter(|&e| *e < maxdist).count()
}

/// Each index holds a defined Location with the index as id
type Map = Vec<Location>;

/// Each loaction holds its id (its index into a Map) and
/// an indication of the size of its area
#[derive(Debug)]
struct Location {
    id: usize,
    pnt: Point,
    size: AreaSz,
}

/// A point on the map, not necessarily a location.
/// Points with negative cooordinates are not needed
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Point {
    x: usize,
    y: usize,
}

/// A Locations's area is yet Unknown, Infinite or has a Finite area size
#[derive(Default, Debug, Clone, Copy)]
enum AreaSz {
    Infinite,
    Finite(u32),
    #[default]
    Unknown,
}

/// Create a map from the definition lines.
/// Each line gives an x and a y, seperted by ", "
fn make_map(inp: &[&str]) -> Map {
    let mut result: Map = Vec::with_capacity(inp.len());
    for &line in inp {
        let xy: Vec<_> = line
            .split(", ")
            .map(|s| s.parse::<usize>().unwrap())
            .collect();
        result.push(Location {
            id: result.len(),
            pnt: Point { x: xy[0], y: xy[1] },
            size: AreaSz::Unknown,
        });
    }
    result
}

/// Determine the topleft and bottomright point of the map
/// The minimum and maximum x and y are determined.
/// There is no need to look beyond these limits
fn map_size(map: &Map) -> (Point, Point) {
    let mut minx = usize::MAX;
    let mut miny = usize::MAX;
    let mut maxx = usize::MIN;
    let mut maxy = usize::MIN;

    for loc in map {
        minx = minx.min(loc.pnt.x);
        miny = miny.min(loc.pnt.y);
        maxx = maxx.max(loc.pnt.x);
        maxy = maxy.max(loc.pnt.y);
    }
    (Point { x: minx, y: miny }, Point { x: maxx, y: maxy })
}

/// Create a string from the map, for display
fn map_to_string(map: &Map, br: &Point) -> String {
    let mut result: Vec<String> = Vec::new();
    for y in 0..=br.y {
        result.push(
            (0..=br.x)
                .map(|x| {
                    if map.iter().any(|loc| loc.pnt == Point { x, y }) {
                        '#'
                    } else {
                        '.'
                    }
                })
                .collect(),
        );
    }
    result.join("\n")
}

/// For each point on the map from tl to br:
///     Determine the distances to all Locations and see if there is a single minimum.
///     If there is:
///         assign the point to the location:  assigned[y][x] == Some(loc)
///         bump the locotion's size count
///         if the point is on the edge: set an infinity flag
///     If there is not:
///         make the point a tie assigned[y][x] == None
///         do not touch the location's size count
///         do not touch infinity
/// For all locations
///     if any of its points are on the map's edge: set the locations area size to infinity
///     else set to finite with the count.
fn assign_points(map: &mut Map, tl: &Point, br: &Point) {
    // size holds the number of points assigned to this location. Each location starts with 1: its own Point;
    let mut size: Vec<usize> = vec![1; map.len()];
    // infinity holds a boolean saying that this location's area size is infinite
    let mut infinity = vec![false; map.len()];
    // assigned holds the assigned location for each point in the map
    let mut assigned: Vec<Vec<Option<usize>>> = vec![Vec::new(); br.y + 1];
    let locations: HashSet<Point> = map.iter().map(|loc| loc.pnt).collect();
    for y in 0..=br.y {
        assigned[y] = vec![None; br.x + 1];
        for x in 0..=br.x {
            let pnt = Point { x, y };
            if locations.contains(&pnt) {
                continue;
            }
            let distances: Vec<_> = map.iter().map(|loc| loc.pnt.dist_to(&pnt)).collect();
            let min_dist = *distances.iter().min().unwrap();
            let min_cnt = distances.iter().filter(|&e| *e == min_dist).count();
            if min_cnt == 1 {
                let loc = distances.iter().position(|&e| e == min_dist).unwrap();
                size[loc] += 1;
                if x == tl.x || x == br.x || y == tl.y || y == br.y {
                    // debug!(
                    //     "{} is infinite @ ({},{})",
                    //     ('a' as u8 + loc as u8) as char,
                    //     x,
                    //     y
                    // );
                    infinity[loc] = true;
                }
                assigned[y][x] = Some(loc)
            }
        }
    }
    debug!(
        "Assignement map:\n{}",
        assigned_to_string(&map, &assigned, &br)
    );
    for loc_idx in 0..map.len() {
        map[loc_idx].size = if infinity[loc_idx] {
            AreaSz::Infinite
        } else {
            AreaSz::Finite(size[loc_idx] as u32)
        };
    }
}

fn assigned_to_string(map: &Map, assigned: &Vec<Vec<Option<usize>>>, br: &Point) -> String {
    let mut result: Vec<String> = Vec::new();
    for y in 0..=br.y {
        result.push(
            (0..=br.x)
                .map(|x| {
                    if map.iter().any(|loc| loc.pnt == Point { x, y }) {
                        '#'
                    } else if let Some(loc) = assigned[y][x] {
                        ('a' as u8 + loc as u8) as char
                    } else {
                        '.'
                    }
                })
                .collect(),
        );
    }
    result.join("\n")
}

/// For part2, for each point, return the sum of distances to all locations
fn dist_to_all(map: &Map, br: &Point) -> Vec<Vec<usize>> {
    let mut result = vec![Vec::new(); br.y + 1];
    for y in 0..=br.y {
        result[y] = vec![usize::MAX; br.x + 1];
        for x in 0..=br.x {
            let pnt = Point { x, y };
            result[y][x] = map.iter().map(|loc| loc.pnt.dist_to(&pnt)).sum();
        }
    }
    result
}

fn abs_diff(a: usize, b: usize) -> usize {
    if a >= b {
        a - b
    } else {
        b - a
    }
}
impl Point {
    fn dist_to(&self, other: &Point) -> usize {
        abs_diff(self.x, other.x) + abs_diff(self.y, other.y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT01: &str = "\
    1, 1
    1, 6
    8, 3
    3, 4
    5, 5
    8, 9
        ";

    #[test]
    /// Test individual shuffling techniques
    fn test1() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(17, part1(&input));
    }

    #[test]
    fn test2() {
        init_logger();
        let input: Vec<_> = INPUT01
            .lines()
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();
        assert_eq!(16, part2_2(&input, 32));
    }

    fn init_logger() {
        let _ = env_logger::Builder::new()
            .filter_level(LOGLEVEL)
            .format(|buf, record| {
                writeln!(
                    buf,
                    "{:6} [{}]: {}",
                    record.line().unwrap(),
                    record.level(),
                    record.args()
                )
            })
            .is_test(true)
            .try_init();
    }
}
