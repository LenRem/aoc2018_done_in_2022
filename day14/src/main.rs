// Needed use statements for scaffolding
use std::fmt::Display;
use std::io::Write;
use std::time::Instant;

const INPUT: &str = "147061";

fn main() {
    let input = INPUT;
    run_and_report("Part1", &part1, input);
    run_and_report("Part2", &part2, input);
}

fn run_and_report<T, R: Display>(name: &str, f: &dyn Fn(T) -> R, input: T) {
    let now = Instant::now();
    let result = f(input);
    let elapsed = now.elapsed().as_micros() as f64 / 1000.0;
    println!("{name}: {result} in {elapsed} ms");
}

fn part1(input: &str) -> String {
    let input = input
        .chars()
        .fold(0, |acc, c| acc * 10 + c.to_digit(10).unwrap() as usize);

    let mut recipes = "37".to_string();
    let mut elf1 = 0;
    let mut elf2 = 1;
    while recipes.len() < input + 10 as usize {
        let bytestr = recipes.as_bytes();
        let u1 = (bytestr[elf1] as char).to_digit(10).unwrap();
        let u2 = (bytestr[elf2] as char).to_digit(10).unwrap();
        recipes.push_str(&(u1 + u2).to_string());
        elf1 = (elf1 + u1 as usize + 1) % recipes.len();
        elf2 = (elf2 + u2 as usize + 1) % recipes.len();
    }
    recipes[input..input + 10].to_string()
}

fn part2(input: &str) -> usize {
    let mut recipes = "37".to_string();
    let mut elf1 = 0;
    let mut elf2 = 1;
    let mut count = 0;
    let sz = loop {
        if let Some(sz) = ready2(&recipes, input) {
            break sz;
        }
        let bytestr = recipes.as_bytes();
        let u1 = (bytestr[elf1] as char).to_digit(10).unwrap();
        let u2 = (bytestr[elf2] as char).to_digit(10).unwrap();
        recipes.push_str(&(u1 + u2).to_string());
        elf1 = (elf1 + u1 as usize + 1) % recipes.len();
        elf2 = (elf2 + u2 as usize + 1) % recipes.len();
        count += 1;
    };
    println!("{}", count);
    sz
}

fn ready2(recipes: &str, goal: &str) -> Option<usize> {
    let rsz = recipes.len();
    let gsz = goal.len();
    if rsz < gsz + 1 {
        return None;
    }
    let rcp_end = &recipes[rsz - gsz - 1..];
    rcp_end.find(goal).map(|idx| rsz - gsz - 1 + idx)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    /// Test individual shuffling techniques
    fn test1a() {
        let input = "9";
        assert_eq!("5158916779", part1(input));
    }

    #[test]
    /// Test individual shuffling techniques
    fn test1b() {
        let input = "5";
        assert_eq!("0124515891", part1(input));
    }

    #[test]
    /// Test individual shuffling techniques
    fn test1c() {
        let input = "18";
        assert_eq!("9251071085", part1(input));
    }

    #[test]
    /// Test individual shuffling techniques
    fn test1d() {
        let input = "2018";
        assert_eq!("5941429882", part1(input));
    }

    #[test]
    fn test2a() {
        let input = "51589";
        assert_eq!(9, part2(input));
    }

    #[test]
    fn test2b() {
        let input = "01245";
        assert_eq!(5, part2(input));
    }

    #[test]
    fn test2c() {
        let input = "92510";
        assert_eq!(18, part2(input));
    }

    #[test]
    fn test2d() {
        let input = "59414";
        assert_eq!(2018, part2(input));
    }

}
